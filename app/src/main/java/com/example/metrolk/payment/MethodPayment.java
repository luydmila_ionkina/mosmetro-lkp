package com.example.metrolk.payment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.adapters.MessageAdapter;
import com.example.metrolk.adapters.MethodPaymentAdapter;

public class MethodPayment extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_method_payment);

        Toolbar toolbar = findViewById(R.id.toolbar);

        ImageView back = toolbar.findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView textBar = findViewById(R.id.textBar);
        textBar.setText(R.string.method_payment);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        RecyclerView CardView = findViewById(R.id.CardView);
        CardView.setLayoutManager(linearLayoutManager);
        CardView.setAdapter(new MethodPaymentAdapter(this));
    }

    public void addCard(View view) {
        Intent intent = new Intent(this, AddCard.class);
        intent.putExtra("type", StaticData.ActivityType.addBankCard);
        startActivity(intent);
    }
}
