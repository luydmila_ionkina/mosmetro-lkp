package com.example.metrolk.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.communication.ConversationMM;

public class AppealAdapter extends RecyclerView.Adapter<AppealAdapter.ViewHolder>{

    private LayoutInflater inflater;
    private Context context;

    AppealAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public AppealAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.appeal, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AppealAdapter.ViewHolder holder, final int position) {
        if(position == 0) {
            holder.state.setText(R.string.sent);
            holder.state.setTextColor(StaticData.colorRed);

            holder.content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ConversationMM.class);
                    intent.putExtra("type", "sent");
                    context.startActivity(intent);
                }
            });
        }
        if(position == 1) {
            holder.state.setText(R.string.answered);
            holder.state.setTextColor(StaticData.colorGreen);

            holder.content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ConversationMM.class);
                    intent.putExtra("type", "answered");
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView state;
        LinearLayout content;

        ViewHolder(View view) {
            super(view);

            state = view.findViewById(R.id.state);
            content = view.findViewById(R.id.Content);
        }
    }
}
