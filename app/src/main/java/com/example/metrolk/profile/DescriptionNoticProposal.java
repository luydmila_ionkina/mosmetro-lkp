package com.example.metrolk.profile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;

public class DescriptionNoticProposal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description_notic_proposal);

        Toolbar toolbar = findViewById(R.id.toolbar);

        ImageView back = toolbar.findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        StaticData.News type = (StaticData.News) getIntent().getSerializableExtra("type");
        String title_bar = null;

        if(type != null) {
            switch(type) {
                case notification: title_bar = "Уведомление";
                    break;
                case proposal:
                    title_bar = "Предложение";

                    ImageView image = findViewById(R.id.image);
                    image.setVisibility(View.VISIBLE);
                    //image.setImageResource(R.drawable.example_cart);
                    break;
            }
        }

        TextView textBar = findViewById(R.id.textBar);
        if(title_bar != null) textBar.setText(title_bar);

        String title = getIntent().getStringExtra("title");

        TextView titleView = findViewById(R.id.title);
        if(title != null) titleView.setText(title);
    }
}
