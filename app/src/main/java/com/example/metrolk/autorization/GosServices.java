package com.example.metrolk.autorization;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.registration.CreateUserAccount;
import com.example.metrolk.registration.Registration;

public class GosServices extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gos_services);

        final StaticData.ActivityType type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case socialNetwork:
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try { Thread.sleep(3000); }
                            catch (InterruptedException e) { e.printStackTrace(); }

                            GosServices.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Intent intent = new Intent(GosServices.this, CreateUserAccount.class);
                                        intent.putExtra("type", type);
                                        startActivity(intent);
                                    } catch (Exception e) { }
                                }
                            });

                        }
                    }).start();
                    break;
                case mos_ru:
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try { Thread.sleep(3000); }
                            catch (InterruptedException e) { e.printStackTrace(); }

                            GosServices.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        Intent intent = new Intent(GosServices.this, EnterCode.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.putExtra("type", StaticData.ActivityType.newCode);
                                        startActivity(intent);
                                    } catch (Exception e) { }
                                }
                            });

                        }
                    }).start();
                    break;
            }
        }
    }

    public void onClickBack(View view) {
        finish();
    }
}
