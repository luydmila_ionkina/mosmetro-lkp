package com.example.metrolk.autorization;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;

public class EnterPAccount extends AppCompatActivity {

    private int position = 0;
    private String code = "";
    private ImageView imageCode1, imageCode2, imageCode3, imageCode4, imageCode5, imageDel;
    private ImageView codeView[];
    private TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_paccount);

        imageCode1 = findViewById(R.id.imageCode1);
        imageCode2 = findViewById(R.id.imageCode2);
        imageCode3 = findViewById(R.id.imageCode3);
        imageCode4 = findViewById(R.id.imageCode4);
        imageCode5 = findViewById(R.id.imageCode5);
        codeView = new ImageView[]{imageCode1, imageCode2, imageCode3, imageCode4, imageCode5};

        imageDel = findViewById(R.id.delete);
        imageDel.setEnabled(true);
        text = findViewById(R.id.text);
    }

    public void onPress1(View view) {
        setCode("1");
    }

    public void onPress2(View view) {
        setCode("2");
    }

    public void onPress3(View view) {
        setCode("3");
    }

    public void onPress4(View view) {
        setCode("4");
    }

    public void onPress5(View view) {
        setCode("5");
    }

    public void onPress6(View view) {
        setCode("6");
    }

    public void onPress7(View view) {
        setCode("7");
    }

    public void onPress8(View view) {
        setCode("8");
    }

    public void onPress9(View view) {
        setCode("9");
    }

    public void onPress0(View view) {
        setCode("0");
    }

    private void setCode(String c) {
        if (position < 5) {
            if (position < 0) {
                imageDel.setColorFilter(StaticData.colorGray);
                imageDel.setEnabled(false);
            } else {
                imageDel.setColorFilter(StaticData.colorRed);
                imageDel.setEnabled(true);
            }
            code = code + c;
            codeView[position].setBackgroundColor(StaticData.colorRed);
            position++;
        }
        if (position == 5) {
            if (code.equals("12345")) {
                //
            } else {
                text.setText(R.string.enter_code_5);

                for (int i = 0; i < codeView.length; i++)
                    codeView[i].setBackgroundColor(StaticData.colorGray);

                code = "";
                position = 0;
            }
        }
    }

    public void delCode(View view) {
        if ((position > 0) & (position < 5)) {
            imageDel.setColorFilter(StaticData.colorRed);
            imageDel.setEnabled(true);

            code = code.substring(0, code.length() - 1);
            codeView[position - 1].setBackgroundColor(StaticData.colorGray);
            position--;
        }
        if (position <= 0) {
            imageDel.setColorFilter(StaticData.colorGray);
            imageDel.setEnabled(false);
        }
    }
}
