package com.example.metrolk.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.recovery.MethodRecovery;
import com.example.metrolk.recovery.RecoveryAccess;

public class EnterNameCard extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization);

        findViewById(R.id.enter).requestFocus();

        ImageView question = findViewById(R.id.question);
        question.setEnabled(true);
        question.setVisibility(View.INVISIBLE);

        TextView title = findViewById(R.id.title);
        title.setText(R.string.add_card);

        TextView text = findViewById(R.id.text);
        text.setText(R.string.enter_card_name);

        final TextView loginHint = findViewById(R.id.loginHint);
        loginHint.setText(R.string.example_5);

        final EditText passwordText = findViewById(R.id.enter);
        passwordText.setInputType(InputType.TYPE_CLASS_TEXT);
        passwordText.setHint(R.string.your_card_name);

        LinearLayout content = findViewById(R.id.Content);
        content.setVisibility(View.GONE);

        TextView recovery = findViewById(R.id.recovery);
        recovery.setVisibility(View.INVISIBLE);
        recovery.setEnabled(false);

        final Button buttonNext = findViewById(R.id.buttonNext);
        final LinearLayout divider = findViewById(R.id.divider);

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (passwordText.length() > 0) {
                    buttonNext.setEnabled(true);
                    buttonNext.setTextColor(StaticData.colorWhite);
                    buttonNext.setBackgroundResource(R.drawable.round_message_red);
                    loginHint.setVisibility(View.VISIBLE);
                    divider.setBackgroundColor(StaticData.colorRed);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (2 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                } else {
                    buttonNext.setEnabled(false);
                    buttonNext.setTextColor(StaticData.colorGrayText);
                    buttonNext.setBackgroundResource(R.drawable.round_gray);
                    loginHint.setVisibility(View.INVISIBLE);
                    divider.setBackgroundColor(StaticData.colorGray);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (1 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        passwordText.addTextChangedListener(watcher);
    }

    public void onClickNext(View view) {
        Intent intent = new Intent(this, MethodRecovery.class);
        intent.putExtra("type", StaticData.ActivityType.addTransportCard);
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }
}
