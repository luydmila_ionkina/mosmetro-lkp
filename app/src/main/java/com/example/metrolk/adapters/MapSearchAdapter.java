package com.example.metrolk.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.metrolk.R;

public class MapSearchAdapter extends RecyclerView.Adapter<MapSearchAdapter.ViewHolder> {

    private final Context mContext;
    private LayoutInflater inflater;

    public MapSearchAdapter(Context c) {
        this.mContext = c;
        this.inflater = LayoutInflater.from(mContext);
    }

    @Override
    public MapSearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.search_field, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MapSearchAdapter.ViewHolder holder, final int position) {
        //
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ViewHolder(View view) {
            super(view);
        }
    }
}