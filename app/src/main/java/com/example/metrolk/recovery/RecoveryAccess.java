package com.example.metrolk.recovery;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.MainActivity;
import com.example.metrolk.PhoneEditText;
import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;
import com.example.metrolk.communication.ConfirmSituation;
import com.example.metrolk.registration.CreateUserCode;

public class RecoveryAccess extends AppCompatActivity {

    TextView loginHint;
    Button buttonNext;
    EditText enterEmail;
    PhoneEditText enterPhone;
    LinearLayout divider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization);

        ImageView close = findViewById(R.id.close);
        close.setImageResource(R.drawable.ic_back);

        ImageView question = findViewById(R.id.question);
        question.setEnabled(true);
        question.setVisibility(View.INVISIBLE);

        TextView title = findViewById(R.id.title);
        TextView text = findViewById(R.id.text);
        loginHint = findViewById(R.id.loginHint);
        buttonNext = findViewById(R.id.buttonNext);
        enterEmail = findViewById(R.id.enter);
        enterPhone = findViewById(R.id.enter_tel);
        divider = findViewById(R.id.divider);

        LinearLayout content = findViewById(R.id.Content);
        content.setVisibility(View.GONE);

        TextView recovery = findViewById(R.id.recovery);
        recovery.setVisibility(View.INVISIBLE);
        recovery.setEnabled(false);

        StaticData.ActivityType type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case recoveryAccess:
                    enterPhone.setVisibility(View.VISIBLE);
                    enterPhone.setContext(this);
                    enterEmail.setVisibility(View.GONE);
                    recoveryAccess(title, text, loginHint, enterEmail);
                    break;
                case renameCard: renameCard(title, text, loginHint, enterEmail, buttonNext);
                    break;
            }
        }

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (enterEmail.length() > 0) activateButton();
                else disactivateButton();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        enterEmail.addTextChangedListener(watcher);

         /*StaticData.showDialog(getString(R.string.user_not_found),
                getString(R.string.user_not_found_2), getString(R.string.close), null, this);*/
    }

    public void activateButton() {
        buttonNext.setEnabled(true);
        buttonNext.setTextColor(StaticData.colorWhite);
        buttonNext.setBackgroundResource(R.drawable.round_message_red);
        loginHint.setVisibility(View.VISIBLE);
        divider.setBackgroundColor(StaticData.colorRed);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.height = (int) (2 * getResources().getDisplayMetrics().density + 0.5f);
        divider.setLayoutParams(layoutParams);
    }

    public void disactivateButton() {
        buttonNext.setEnabled(false);
        buttonNext.setTextColor(StaticData.colorGrayText);
        buttonNext.setBackgroundResource(R.drawable.round_gray);
        loginHint.setVisibility(View.INVISIBLE);
        divider.setBackgroundColor(StaticData.colorGray);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.height = (int) (1 * getResources().getDisplayMetrics().density + 0.5f);
        divider.setLayoutParams(layoutParams);
    }

    public void recoveryAccess(TextView title, TextView text, TextView loginHint, EditText passwordText) {
        title.setText(R.string.recovery_state);
        text.setText(R.string.enter_mail_phone);
        loginHint.setText(R.string.login);

        LinearLayout layoutPhoneEmail = findViewById(R.id.EnterPhoneEmail);
        layoutPhoneEmail.setVisibility(View.VISIBLE);

        final RoundButton phone = findViewById(R.id.phone);
        final RoundButton email = findViewById(R.id.email);

        enterEmail.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticData.activateRoundButton(phone);
                StaticData.disactivateRoundButton(email);

                enterPhone.setVisibility(View.VISIBLE);
                enterEmail.setVisibility(View.GONE);

                if(enterPhone.getText().length() == 18) activateButton();
                else disactivateButton();
            }
        });

        email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticData.activateRoundButton(email);
                StaticData.disactivateRoundButton(phone);

                enterEmail.setVisibility(View.VISIBLE);
                enterPhone.setVisibility(View.GONE);

                if(enterEmail.getText().length() > 0) activateButton();
                else disactivateButton();
            }
        });

        passwordText.setHint(R.string.login);
        passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
    }

    public void renameCard(TextView title, TextView text, TextView loginHint, EditText passwordText, Button buttonNext) {
        title.setText(R.string.rename_card);
        text.setText(R.string.enter_new_card_name);
        loginHint.setText(R.string.example_5);

        passwordText.setHint(R.string.your_card_name);
        passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);

        buttonNext.setText(R.string.save);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = MainActivity.mSettings.edit();
                editor.putString(MainActivity.AUTORIZATION, "authorized");
                editor.apply();

                Intent intent = new Intent(RecoveryAccess.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("type", StaticData.ActivityType.authorized);
                startActivity(intent);
            }
        });
    }

    public void onClickNext(View view) {
        /*Intent intent = new Intent(this, MethodRecovery.class);
        intent.putExtra("type", StaticData.ActivityType.recoveryPassword);
        startActivity(intent);*/

        Intent intent = new Intent(RecoveryAccess.this, CreateUserCode.class);
        intent.putExtra("type", StaticData.ActivityType.recoveryPassword);
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }
}
