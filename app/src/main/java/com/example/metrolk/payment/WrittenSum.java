package com.example.metrolk.payment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.metrolk.MainActivity;
import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;
import com.example.metrolk.profile.CardConnected;

public class WrittenSum extends AppCompatActivity {

    float density;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_written_sum);

        density = getResources().getDisplayMetrics().density;
        writtingSum();
    }

    public void writtingSum() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try { Thread.sleep(3000); }
                catch (InterruptedException e) { e.printStackTrace(); }

                WrittenSum.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            ImageView image = findViewById(R.id.image);
                            image.setVisibility(View.INVISIBLE);

                            ProgressBar progressBar = findViewById(R.id.progress);
                            progressBar.setVisibility(View.VISIBLE);
                        } catch (Exception e) { }
                    }
                });

                try { Thread.sleep(3000); }
                catch (InterruptedException e) { e.printStackTrace(); }

                WrittenSum.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            TextView title = findViewById(R.id.title);
                            title.setText(R.string.written_success);

                            TextView text = findViewById(R.id.text);
                            text.setText(R.string.written_success_2);

                            ProgressBar progressBar = findViewById(R.id.progress);
                            progressBar.setVisibility(View.GONE);

                            ImageView image = findViewById(R.id.image);
                            image.setVisibility(View.GONE);

                            ImageView imageDone = findViewById(R.id.imageDone);
                            imageDone.setVisibility(View.VISIBLE);

                            RoundButton next = findViewById(R.id.next);
                            next.setVisibility(View.VISIBLE);
                            next.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(WrittenSum.this, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            });

                        } catch (Exception e) { }
                    }
                });
            }
        }).start();
    }
}
