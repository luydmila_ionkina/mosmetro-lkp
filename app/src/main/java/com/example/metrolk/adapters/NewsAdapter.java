package com.example.metrolk.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.profile.DescriptionNoticProposal;
import com.example.metrolk.profile.DescriptionPoll;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder>
        implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private LayoutInflater inflater;
    private Context mContext;
    private StaticData.News type;
    private String titleAll[] = { "Ваш запрос в обработке", "Что вы думаете о новых станциях Московского метрополитена?",
            "Спецпредложение по покупке билетов", "Ваш запрос в обработке", "Ваш запрос в обработке" };
    private String titleNotic[] = { "Ваш запрос в обработке" };
    private String titlePoll[] = { "Что вы думаете о новых станциях Московского метрополитена?" };
    private String titleProposal[] = { "Спецпредложение по покупке билетов" };
    private int count;

    NewsAdapter(Context context, StaticData.News type, int c) {
        mContext = context;
        this.inflater = LayoutInflater.from(context);
        this.type = type;
        count = c;
    }

    @Override
    public NewsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.empy_container, parent, false);

        if(type != null) {
            switch(type) {
                case all:
                    break;
                case poll: view = inflater.inflate(R.layout.poll, parent, false);
                    break;
                case proposal: view = inflater.inflate(R.layout.proposal, parent, false);
                    break;
                case notification: view = inflater.inflate(R.layout.notification, parent, false);
                    break;
            }
        }
        return new ViewHolder(view);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof NewsAdapter.ViewHolder) {

            final int deletedIndex = viewHolder.getAdapterPosition();

        }
    }

    @Override
    public void onBindViewHolder(final NewsAdapter.ViewHolder holder, final int position) {
        if(type != null) {
            switch(type) {
                case all: createAllNews(holder, position);
                    break;
                case poll:
                    holder.layoutContent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onClickPoll(position);
                        }
                    });
                    break;
                case proposal:
                    holder.layoutContent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onClickProposal(position);
                        }
                    });
                    break;
                case notification:
                    holder.layoutContent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onClickNotic(position);
                        }
                    });
                    break;
            }
        }
    }

    public void createAllNews(final NewsAdapter.ViewHolder holder, final int position) {
        View view;

        holder.viewBackground.setPadding((int) (24 * mContext.getResources().getDisplayMetrics().density + 0.5f), 0,
                (int) (24 * mContext.getResources().getDisplayMetrics().density + 0.5f), 0);

        if (position == 0) {
            view = inflater.inflate(R.layout.notification, null, false);
            holder.layoutContent.removeAllViews();
            holder.layoutContent.addView(view);

            holder.layoutContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   onClickNotic(position);
                }
            });
        }
        if (position == 1) {
            view = inflater.inflate(R.layout.poll, null, false);
            holder.layoutContent.removeAllViews();
            holder.layoutContent.addView(view);

            holder.layoutContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickPoll(position);
                }
            });
        }
        if (position == 2) {
            view = inflater.inflate(R.layout.proposal, null, false);
            holder.layoutContent.removeAllViews();
            holder.layoutContent.addView(view);

            holder.layoutContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   onClickProposal(position);
                }
            });
        }
        if (position > 2) {
            view = inflater.inflate(R.layout.notification, null, false);
            holder.layoutContent.removeAllViews();
            holder.layoutContent.addView(view);

            holder.layoutContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickNotic(position);
                }
            });
        }
    }

    public void onClickNotic(int position) {
        Intent intent = new Intent(mContext, DescriptionNoticProposal.class);
        intent.putExtra("type", StaticData.News.notification);

        switch(type) {
           case all: intent.putExtra("title", titleAll[position]);
           break;
           case notification: intent.putExtra("title", titleNotic[0]);
           break;
        }
        mContext.startActivity(intent);
    }

    public void onClickProposal(int position) {
        Intent intent = new Intent(mContext, DescriptionNoticProposal.class);
        intent.putExtra("type", StaticData.News.proposal);

        switch(type) {
            case all: intent.putExtra("title", titleAll[position]);
            break;
            case proposal: intent.putExtra("title", titleProposal[0]);
            break;
        }
        mContext.startActivity(intent);
    }

    public void onClickPoll(int position) {
        Intent intent = new Intent(mContext, DescriptionPoll.class);
        intent.putExtra("type", StaticData.News.poll);

        switch(type) {
            case all: intent.putExtra("title", titleAll[position]);
            break;
            case poll: intent.putExtra("title", titlePoll[0]);
            break;
        }

        if(position % 2 == 0) intent.putExtra("checklist", StaticData.Checklist.checklist);
        else intent.putExtra("checklist", StaticData.Checklist.radioGroup);
        mContext.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return count;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layoutContent;
        public RelativeLayout viewBackground, viewForeground;
        TextView title;

        ViewHolder(View view) {
            super(view);

            layoutContent = view.findViewById(R.id.Content);
            viewBackground = view.findViewById(R.id.view_background);
            viewForeground = view.findViewById(R.id.view_foreground);
            title = view.findViewById(R.id.title);
        }
    }
}