package com.example.metrolk.communication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.metrolk.R;

public class Communication extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_communication);

        Toolbar toolbar = findViewById(R.id.toolbar);

        ImageView back = toolbar.findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView textBar = findViewById(R.id.textBar);
        textBar.setText(R.string.contact);
    }

    public void onClickConversationMM(View view) {
        startActivity(new Intent(this, ConversationMM.class));
    }

    public void onClickCallAccompanying(View view) {
        Intent intent = new Intent(this, ConversationMM.class);
        intent.putExtra("type", "accompanying");
        startActivity(intent);
    }
}
