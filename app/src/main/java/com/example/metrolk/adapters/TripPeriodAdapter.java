package com.example.metrolk.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.communication.ConversationMM;
import com.example.metrolk.payment.AddCardScore;
import com.example.metrolk.tariff.Tariff;

public class TripPeriodAdapter extends RecyclerView.Adapter<TripPeriodAdapter.ViewHolder>{

    private LayoutInflater inflater;
    private Context context;
    private int count;
    private StaticData.Element type;

    public TripPeriodAdapter(Context context, int count, StaticData.Element t) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.count = count;
        this.type = t;
    }

    @Override
    public TripPeriodAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.tariff_trip_period, parent, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(context.getClass() == AddCardScore.class) {
                    AddCardScore addCardScore = (AddCardScore) context;
                    addCardScore.selectCard3();
                }
            }
        });

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TripPeriodAdapter.ViewHolder holder, final int position) {
        if(type != null) {
            switch(type) {
                case trip: createCountTripView(holder);
                    break;
                case period: createPeriodTripView(holder);
                    break;
            }
        }
    }

    public void createCountTripView(TripPeriodAdapter.ViewHolder holder) {

    }

    public void createPeriodTripView(TripPeriodAdapter.ViewHolder holder) {
        holder.name.setText("Единый - 30 дней");
    }

    @Override
    public int getItemCount() {
        return count;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;

        ViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.name);
        }
    }
}
