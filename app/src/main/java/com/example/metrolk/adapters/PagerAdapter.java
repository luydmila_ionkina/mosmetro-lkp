package com.example.metrolk.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.metrolk.StaticData;

public class PagerAdapter extends FragmentPagerAdapter {

    final int count;
    private Context context;
    private String tabTitles[];
    private float density;
    private StaticData.ActivityType typeContent;

    public PagerAdapter(FragmentManager fm, Context c, String t[], float d, StaticData.ActivityType type) {
        super(fm);
        context = c;
        count = t.length;
        density = d;
        typeContent = type;
        tabTitles = new String[count];

        for (int i = 0; i < t.length; i++)
            tabTitles[i] = t[i];
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public Fragment getItem(int position) {
        return PageFragment.newInstance(context, position, density, typeContent);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}