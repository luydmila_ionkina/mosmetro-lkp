package com.example.metrolk.communication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.MainActivity;
import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;
import com.example.metrolk.registration.ConfirmationRecovery;

public class ConfirmSituation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_situation);

        String state = getIntent().getStringExtra("message");
        if((state != null) && (state.equals("sent"))) {

            ImageView close = findViewById(R.id.close);
            close.setVisibility(View.INVISIBLE);
            close.setEnabled(false);

            TextView title = findViewById(R.id.title);
            title.setText(R.string.appeal_sent);

            TextView text = findViewById(R.id.text);
            text.setText(R.string.appeal_sent_2);

            LinearLayout layoutPhoto = findViewById(R.id.layoutPhoto);
            layoutPhoto.setVisibility(View.GONE);

            RoundButton next = findViewById(R.id.next);
            next.setButtonText(getString(R.string.back_to_lkp));
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences.Editor editor = MainActivity.mSettings.edit();
                    editor.putString(MainActivity.AUTORIZATION, "authorized");
                    editor.apply();

                    Intent intent = new Intent(ConfirmSituation.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("type", StaticData.ActivityType.authorized);
                    startActivity(intent);
                }
            });
        }
    }

    public void onClickNext(View view) {
        Intent intent = new Intent(this, ConfirmSituation.class);
        intent.putExtra("message", "sent");
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }
}
