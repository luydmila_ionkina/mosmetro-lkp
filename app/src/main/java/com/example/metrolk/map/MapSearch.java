package com.example.metrolk.map;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.adapters.MapSearchAdapter;
import com.example.metrolk.adapters.MethodPaymentAdapter;
import com.example.metrolk.adapters.TerminalAdapter;

public class MapSearch extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_search);

        RecyclerView searchView = findViewById(R.id.Search);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        searchView.setLayoutManager(linearLayoutManager);

        StaticData.Map type = (StaticData.Map) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case map: searchView.setAdapter(new MapSearchAdapter(this));
                    break;
                case terminal: terminal(searchView);
                    break;
            }
        }
    }

    public void terminal(RecyclerView searchView) {
        ImageView back = findViewById(R.id.back);
        back.setImageResource(R.drawable.ic_close);

        EditText searchText = findViewById(R.id.searchText);
        searchText.setVisibility(View.GONE);

        TextView textBar = findViewById(R.id.textBar);
        textBar.setVisibility(View.VISIBLE);

        ImageView icon = findViewById(R.id.icon);
        icon.setImageResource(R.drawable.ic_search);
        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapSearch.this, MapSearch.class);
                intent.putExtra("type", StaticData.Map.map);
                startActivity(intent);
            }
        });

        TextView searching_result = findViewById(R.id.searching_result);
        searching_result.setVisibility(View.GONE);

        float density = getResources().getDisplayMetrics().density;
        searchView.setPadding((int) (0 * density + 0.5f), (int) (24 * density + 0.5f), (int) (0 * density + 0.5f), (int) (0 * density + 0.5f));
        searchView.setAdapter(new TerminalAdapter(this, StaticData.Terminal.searchTerminal));
    }

    public void onClickClearText(View view){
        EditText searchText = findViewById(R.id.searchText);
        searchText.setText("");
    }

    public void onClickBack(View view) {
        finish();
    }
}
