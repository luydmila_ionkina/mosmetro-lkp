package com.example.metrolk.registration;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.MainActivity;
import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.autorization.EnterCode;
import com.example.metrolk.payment.WrittenSumMetro;
import com.example.metrolk.profile.Profile;

public class ConfirmationRecovery extends AppCompatActivity {

    StaticData.ActivityType type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization);

        findViewById(R.id.enter).requestFocus();

        ImageView close = findViewById(R.id.close);
        close.setEnabled(false);
        close.setVisibility(View.INVISIBLE);

        ImageView question = findViewById(R.id.question);
        question.setEnabled(true);
        question.setVisibility(View.INVISIBLE);

        TextView title = findViewById(R.id.title);
        title.setText(R.string.reserv_recovery);

        LinearLayout content = findViewById(R.id.Content);
        content.setVisibility(View.GONE);

        TextView text = findViewById(R.id.text);
        String send_code = getString(R.string.code_from_mail);
        if (getIntent().getStringExtra("send_code") != null)
            send_code += " " + getIntent().getStringExtra("send_code");

        TextView recovery = findViewById(R.id.recovery);
        recovery.setText(R.string.code_send_with_error);
        recovery.setEnabled(false);

        final TextView loginHint = findViewById(R.id.loginHint);
        loginHint.setText(R.string.confirmation_code);

        final Button buttonNext = findViewById(R.id.buttonNext);
        buttonNext.setEnabled(false); //
        buttonNext.setVisibility(View.INVISIBLE); //

        final EditText passwordText = findViewById(R.id.enter);
        passwordText.setHint(R.string.confirmation_code);
        passwordText.setInputType(InputType.TYPE_CLASS_NUMBER);

        final LinearLayout divider = findViewById(R.id.divider);

        type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case createUserAccount:
                    title.setText(R.string.create_user_account);

                    close.setVisibility(View.VISIBLE);
                    close.setEnabled(true);
                    close.setImageResource(R.drawable.ic_back);
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                    break;
                case reservRecovery:
                    break;
                case recovery:
                    close.setEnabled(true);
                    close.setVisibility(View.VISIBLE);
                    close.setImageResource(R.drawable.ic_back);

                    title.setText(R.string.recovery_state);
                    send_code = getString(R.string.code_from_mail) + " " + "abc********ru";
                    break;
                case reservMethod:
                    close.setEnabled(true);
                    close.setVisibility(View.VISIBLE);
                    close.setImageResource(R.drawable.ic_back);

                    recovery.setVisibility(View.INVISIBLE);
                    break;
                case changeEmail:
                    title.setText(R.string.security_lk);

                    close.setVisibility(View.VISIBLE);
                    close.setEnabled(true);
                    close.setImageResource(R.drawable.ic_back);
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                    break;
            }
        }
        text.setText(send_code);

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (passwordText.length() > 0) {
                    if (passwordText.length() == 4) {

                        if(type != null) {
                            switch(type) {
                                case createUserAccount:
                                    startActivity(new Intent(ConfirmationRecovery.this, CreateUserPassword.class));
                                    break;
                                case reservRecovery:
                                    Intent intent = new Intent(ConfirmationRecovery.this, WrittenSumMetro.class);
                                    intent.putExtra("type", StaticData.ActivityType.recovery);
                                    startActivity(intent);
                                    break;
                                case recovery:
                                    StaticData.showDialog(getString(R.string.error), getString(R.string.error_invalid_code_2),
                                            getString(R.string.close), null, ConfirmationRecovery.this);
                                    break;
                                case reservMethod:
                                    startActivity(new Intent(ConfirmationRecovery.this, Profile.class));
                                    /*Intent intent = new Intent(ConfirmationRecovery.this, MainActivity.class);
                                    intent.putExtra("type", StaticData.ActivityType.authorized);
                                    startActivity(intent); */
                                    break;
                                case changeEmail:
                                    View.OnClickListener buttonClick = new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            SharedPreferences.Editor editor = MainActivity.mSettings.edit();
                                            editor.putString(MainActivity.AUTORIZATION, "authorized");
                                            editor.apply();

                                            Intent intent = new Intent(ConfirmationRecovery.this, MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            intent.putExtra("type", StaticData.ActivityType.authorized);
                                            startActivity(intent);
                                        }
                                    };

                                    StaticData.showDialog(getString(R.string.phone_change),
                                            getString(R.string.phone_change_2),
                                            getString(R.string.show_personal_account), buttonClick, ConfirmationRecovery.this);
                                    break;
                            }
                        }
                        else {
                            Intent intent = new Intent(ConfirmationRecovery.this, EnterCode.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.putExtra("type", StaticData.ActivityType.newCode);
                            startActivity(intent);
                        }
                    }
                    loginHint.setVisibility(View.VISIBLE);
                    divider.setBackgroundColor(StaticData.colorRed);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (2 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                } else {
                    loginHint.setVisibility(View.INVISIBLE);
                    divider.setBackgroundColor(StaticData.colorGray);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (1 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        passwordText.addTextChangedListener(watcher);
    }

    public void onClickNext(View view) {
        if(type != null) {
            switch(type) {
                case recovery:
                    break;
                case reservMethod:
                    break;
            }
        }
    }

    public void onClickBack(View view) {
        finish();
    }
}
