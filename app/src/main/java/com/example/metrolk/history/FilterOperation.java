package com.example.metrolk.history;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;
import com.example.metrolk.adapters.DateAdapter;
import com.example.metrolk.adapters.DateHistoryAdapter;
import com.example.metrolk.adapters.MessageAdapter;

public class FilterOperation extends AppCompatActivity {

    StaticData.History type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_operation);

        Toolbar toolbar = findViewById(R.id.toolbar);

        ImageView back = toolbar.findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView textBar = findViewById(R.id.textBar);

        type = (StaticData.History) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case trip: textBar.setText(R.string.history_trip);
                trip();
                    break;
                case operation: textBar.setText(R.string.history_operations);
                operation();
                    break;
            }
        }
    }

    public void trip() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        RecyclerView HistoryTrip = findViewById(R.id.HistoryTrip);
        HistoryTrip.setLayoutManager(linearLayoutManager);
        HistoryTrip.setAdapter(new DateHistoryAdapter(this, type, 1));
    }

    public void operation() {
        CardView count = findViewById(R.id.count);
        count.setVisibility(View.VISIBLE);

        LinearLayout layoutUpdate = findViewById(R.id.layoutUpdate);
        layoutUpdate.setVisibility(View.GONE);

        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        RecyclerView HistoryOperation = findViewById(R.id.HistoryTrip);
        HistoryOperation.setLayoutManager(linearLayoutManager2);
        HistoryOperation.setAdapter(new DateHistoryAdapter(this, type, 2));
    }

    public void onClickFilterDescription(View view) {
        Intent intent = new Intent(this, FilterDescription.class);
        if(type != null) intent.putExtra("type", type);
        startActivity(intent);
    }
}