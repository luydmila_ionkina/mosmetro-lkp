package com.example.metrolk.profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.MainActivity;
import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;
import com.example.metrolk.registration.ConfirmationRecovery;

public class CardConnected extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_situation);

        ImageView close = findViewById(R.id.close);
        close.setVisibility(View.INVISIBLE);
        close.setEnabled(false);

        TextView title = findViewById(R.id.title);
        title.setText(R.string.card_connected);

        TextView textView = findViewById(R.id.text);

        String text = "Карта Тройка <b>№ 1234 567 890</b> успешно привязана к вашему аккаунту." +
                " <br></br><br></br>Пополняйте баланс, настраивайте автоплатежи, отслеживайте историю трат и многое другое.";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            textView.setText(Html.fromHtml(text,  Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        else textView.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        LinearLayout layoutPhoto = findViewById(R.id.layoutPhoto);
        layoutPhoto.setVisibility(View.GONE);

        RoundButton next = findViewById(R.id.next);
        next.setButtonText(getString(R.string.back_to_lkp));
    }

    public void onClickNext(View view) {
        SharedPreferences.Editor editor = MainActivity.mSettings.edit();
        editor.putString(MainActivity.AUTORIZATION, "authorized");
        editor.apply();

        Intent intent = new Intent(CardConnected.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("type", StaticData.ActivityType.authorized);
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }
}
