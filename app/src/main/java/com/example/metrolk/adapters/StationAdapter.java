package com.example.metrolk.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.metrolk.R;

public class StationAdapter extends RecyclerView.Adapter<StationAdapter.ViewHolder> {

    private final Context mContext;
    private LayoutInflater inflater;
    private String name[] = {"Калужская", "Кантемировская", "Каховская", "Каширская", "Каширская"};

    public StationAdapter(Context c) {
        this.mContext = c;
        this.inflater = LayoutInflater.from(mContext);
    }

    @Override
    public StationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.station, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StationAdapter.ViewHolder holder, final int position) {
        holder.name.setText(name[position]);
    }

    @Override
    public int getItemCount() {
        return name.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;

        ViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.name);
        }
    }
}