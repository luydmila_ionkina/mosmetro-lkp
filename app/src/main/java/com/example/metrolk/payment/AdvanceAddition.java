package com.example.metrolk.payment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.metrolk.R;
import com.example.metrolk.map.MapPayments;
import com.example.metrolk.payment.AddCardScore;

public class AdvanceAddition extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advance_addition);
    }

    public void onClickMap(View view) {
        startActivity(new Intent(this, MapPayments.class));
    }

    public void onClickAddScore(View view) {
        startActivity(new Intent(this, AddCardScore.class));
    }

    public void onClickBack(View view) {
        finish();
    }
}
