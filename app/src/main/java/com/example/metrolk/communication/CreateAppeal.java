package com.example.metrolk.communication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.RoundButton;

public class CreateAppeal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_appeal);

        String type = getIntent().getStringExtra("type");
        if((type != null) && (type.equals("accompanying"))) {

            TextView title = findViewById(R.id.title);
            title.setText(R.string.path);

            TextView text_1 = findViewById(R.id.text_1);
            text_1.setText(R.string.first_station);

            TextView text_2 = findViewById(R.id.text_2);
            text_2.setText(R.string.last_station);

            LinearLayout layout_3 = findViewById(R.id.Layout_3);
            layout_3.setVisibility(View.GONE);

            RoundButton next = findViewById(R.id.next);
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  Intent intent = new Intent(CreateAppeal.this, EnterPersonalData.class);
                  intent.putExtra("type", "accompanying_2");
                  startActivity(intent);
                }
            });
        }
    }

    public void onClickNext(View view) {
        startActivity(new Intent(this, EnterPersonalData.class));
    }

    public void onClickBack(View view) {
        finish();
    }
}
