package com.example.metrolk.adapters;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;

public class DateAdapter extends RecyclerView.Adapter<DateAdapter.ViewHolder>
        implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private LayoutInflater inflater;
    private Context context;
    private StaticData.News type;
    private String date[] = {"Сегодня", "Вчера", "4 июня, вт"};
    private int count[] = {1, 3, 2};
    private float density;

    DateAdapter(Context context, StaticData.News type, float d) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.type = type;
        density = d;
    }

    @Override
    public DateAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_date, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DateAdapter.ViewHolder holder, final int position) {
        holder.date_str.setText(date[position]);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        holder.recyclerView.setLayoutManager(linearLayoutManager);
        holder.recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        if(type != null){
            switch(type) {
                case notification: holder.recyclerView.setAdapter(new NewsAdapter(context, StaticData.News.notification, count[position]));
                    break;
                case proposal: holder.recyclerView.setAdapter(new NewsAdapter(context, StaticData.News.proposal, count[position]));
                    break;
                case poll: holder.recyclerView.setAdapter(new NewsAdapter(context, StaticData.News.poll, count[position]));
                    break;
            }
        }

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT |
                ItemTouchHelper.RIGHT, this, density);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(holder.recyclerView);
    }

    @Override
    public int getItemCount() {
        return count.length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerView recyclerView;
        TextView date_str;

        ViewHolder(View view) {
            super(view);

            recyclerView = view.findViewById(R.id.News);
            date_str = view.findViewById(R.id.date_str);
        }
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof NewsAdapter.ViewHolder) {

            final int deletedIndex = viewHolder.getAdapterPosition();

        }
    }
}