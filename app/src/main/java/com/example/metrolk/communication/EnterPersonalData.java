package com.example.metrolk.communication;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;

import java.util.Calendar;

public class EnterPersonalData extends AppCompatActivity {

    Calendar dateAndTime = Calendar.getInstance();
    EditText date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_personal_data);

        String type = getIntent().getStringExtra("type");
        if((type != null) && (type.equals("accompanying_2"))) {

            TextView title = findViewById(R.id.title);
            title.setText(R.string.information_travel);

            EditText name = findViewById(R.id.name);
            name.setHint(R.string.time_travel);

            EditText place = findViewById(R.id.address);
            place.setHint(R.string.place_meetting);

            EditText phone = findViewById(R.id.phone);
            phone.setVisibility(View.GONE);

            EditText info = findViewById(R.id.email);
            info.setHint(R.string.more_information);
            info.setInputType(InputType.TYPE_CLASS_TEXT);

            LinearLayout layoutDate = findViewById(R.id.layoutDate);
            layoutDate.setVisibility(View.VISIBLE);
            layoutDate.setBaselineAlignedChildIndex(0);

            LinearLayout divider = findViewById(R.id.divider);
            divider.setVisibility(View.VISIBLE);

            RoundButton next = findViewById(R.id.next);
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   //
                }
            });
        }
        if((type != null) && (type.equals("accompanying"))) {

            EditText address = findViewById(R.id.address);
            address.setVisibility(View.GONE);

            EditText email = findViewById(R.id.email);
            email.setVisibility(View.GONE);

            RoundButton next = findViewById(R.id.next);
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(EnterPersonalData.this, CreateAppeal.class);
                    intent.putExtra("type", "accompanying");
                    startActivity(intent);
                }
            });
        }
        if((type != null) && (type.equals("information"))) {

            TextView title = findViewById(R.id.title);
            title.setText(R.string.info_situation);

            EditText desc_short = findViewById(R.id.name);
            desc_short.setHint(R.string.desc_short_situation);
            desc_short.setInputType(InputType.TYPE_CLASS_TEXT);

            EditText description = findViewById(R.id.address);
            description.setHint(R.string.desc_situation);
            description.setInputType(InputType.TYPE_CLASS_TEXT);

            EditText phone = findViewById(R.id.phone);
            phone.setVisibility(View.GONE);

            EditText email = findViewById(R.id.email);
            email.setVisibility(View.GONE);

            LinearLayout layoutDate = findViewById(R.id.layoutDate);
            layoutDate.setVisibility(View.VISIBLE);

            date = (EditText) findViewById(R.id.Date);

            RoundButton next = findViewById(R.id.next);
            next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(EnterPersonalData.this, ConfirmSituation.class));
                }
            });
        }
    }

    // диалоговое окно для выбора даты
    public void onClickDate(View view) {
        new DatePickerDialog(EnterPersonalData.this, d,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    // установка обработчика выбора даты
    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, monthOfYear);
            dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialDateTime();
        }
    };

    // установка начальных даты и времени
    private void setInitialDateTime() {
        date.setText(DateUtils.formatDateTime(this,
                dateAndTime.getTimeInMillis(), DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR));
    }

    public void onClickNext(View view) {
        Intent intent = new Intent(this, EnterPersonalData.class);
        intent.putExtra("type", "information");
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }
}
