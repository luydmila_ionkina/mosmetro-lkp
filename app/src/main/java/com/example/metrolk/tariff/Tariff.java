package com.example.metrolk.tariff;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.adapters.PagerAdapter;
import com.example.metrolk.adapters.TariffAdapter;
import com.example.metrolk.profile.MessagePanel;

public class Tariff extends AppCompatActivity {

    TextView textBar;
    float density;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tariff);

        density = getResources().getDisplayMetrics().density;

        Toolbar toolbar = findViewById(R.id.toolbar);

        ImageView back = toolbar.findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        textBar = findViewById(R.id.textBar);

        StaticData.ActivityType type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case infoTariff: infoTariff();
                    break;
                case ticketCard: ticketCard();
                    break;
                case autoPayment: autoPayment();
                    break;
            }
        }
    }

    public void infoTariff() {
        String name = getIntent().getStringExtra("name");
        textBar.setText(name);

        String title[] = {"Информация", "Тарифы"};

        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(),
                Tariff.this, title, density, StaticData.ActivityType.infoTariff));

        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void ticketCard() {
        textBar.setText(R.string.tariff);

        String title[] = {"Билеты", "Носители"};

        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(),
                Tariff.this, title, density, StaticData.ActivityType.ticketCard));

        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void autoPayment() {
        textBar.setText(R.string.auto_payment);

        String title[] = {"Основное", "Настройка"};

        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(),
                Tariff.this, title, density, StaticData.ActivityType.autoPayment));

        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void onClickBack(View view) {
        finish();
    }
}
