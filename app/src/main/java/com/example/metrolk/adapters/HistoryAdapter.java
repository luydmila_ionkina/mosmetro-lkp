package com.example.metrolk.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;
import com.example.metrolk.history.HistoryDescription;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    private final Context mContext;
    private LayoutInflater inflater;
    private StaticData.History type;
    private int count;
    private String titleHistoryCard1[] = { "Платеж", "Начисление бонусов" };
    private int iconHistoryCard1[] = { R.drawable.ic_pay, R.drawable.ic_bonus };
    private String textHistoryCard1[] = { "Покупка билета в Московский Зоопарк", "Бонусы по программе лояльности" };
    private String scoreHistoryCard1[] = { "-350 \u20BD", "+35 Б" };
    private int colorScore1[] = { StaticData.colorRed, StaticData.colorGreen };

    private String titleHistoryCard2[] = { "Блокировка карты", "Перенос баланса",
            "Пополнение 1234 567 890", "Списание баллов", "Пополнение 1234 567 890" };
    private int iconHistoryCard2[] = { R.drawable.ic_block, R.drawable.ic_removing,
            R.drawable.ic_payment, R.drawable.ic_bonus, R.drawable.ic_payment };
    private String textHistoryCard2[] = { "Тройка № 4321 567 890", "1234 567 890 > 4321 567 890",
            "**** 1234", "Оплата проезда", "Ожидает записи" };
    private String scoreHistoryCard2[] = { "Выполнено", "Выполнено", "+350 \u20BD", "-38 Б", "+350 \u20BD" };
    private int colorScore2[] = { StaticData.colorGreen, StaticData.colorGreen,
            StaticData.colorGreen, StaticData.colorRed, StaticData.colorGreen };

    public HistoryAdapter(Context c, StaticData.History type, int count) {
        this.mContext = c;
        this.inflater = LayoutInflater.from(mContext);
        this.type = type;
        this.count = count;
    }

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.history_trip, parent, false);

        if(type != null){
            switch(type) {
                case trip: view = inflater.inflate(R.layout.history_trip, parent, false);
                    break;
                case operation: view = inflater.inflate(R.layout.history_operation, parent, false);
                    break;
            }
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryAdapter.ViewHolder holder, final int position) {
        if(type != null){
            switch(type) {
                case trip:
                    holder.Content.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mContext, HistoryDescription.class);
                            intent.putExtra("type", type);
                            mContext.startActivity(intent);
                        }
                    });
                    break;
                case operation: operation(holder, position);
                    break;
            }
        }
    }

    public void operation(HistoryAdapter.ViewHolder holder, final int position) {
        if(count == 2) {
            holder.title.setText(titleHistoryCard1[position]);
            holder.icon.setImageResource(iconHistoryCard1[position]);
            holder.text.setText(textHistoryCard1[position]);
            holder.score.setText(scoreHistoryCard1[position]);
            holder.score.setTextColor(colorScore1[position]);

            if(position == 1) holder.text2.setVisibility(View.VISIBLE);

            holder.Content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, HistoryDescription.class);
                    intent.putExtra("type", type);
                    intent.putExtra("title", titleHistoryCard1[position]);
                    intent.putExtra("text", textHistoryCard1[position]);
                    mContext.startActivity(intent);
                }
            });
        }
        else if(count == 5) {
            holder.title.setText(titleHistoryCard2[position]);
            holder.icon.setImageResource(iconHistoryCard2[position]);
            holder.text.setText(textHistoryCard2[position]);
            holder.score.setText(scoreHistoryCard2[position]);
            holder.score.setTextColor(colorScore2[position]);

            if(position == 1) {
                holder.text.setVisibility(View.GONE);
                holder.layoutPayment.setVisibility(View.VISIBLE);
            }
            if(position == 4) {
                holder.text.setTextColor(StaticData.colorRed);
                holder.button.setVisibility(View.VISIBLE);
            }

            holder.Content.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, HistoryDescription.class);
                    intent.putExtra("type", type);
                    intent.putExtra("title", titleHistoryCard2[position]);
                    intent.putExtra("text", textHistoryCard2[position]);
                    mContext.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
       return count;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView text;
        TextView text2;
        ImageView icon;
        TextView score;
        LinearLayout layoutPayment;
        RoundButton button;
        LinearLayout Content;

        ViewHolder(View view) {
            super(view);

            title = view.findViewById(R.id.title);
            text = view.findViewById(R.id.text);
            text2 = view.findViewById(R.id.text2);
            icon = view.findViewById(R.id.icon);
            score = view.findViewById(R.id.score);
            layoutPayment = view.findViewById(R.id.layoutPayment);
            button = view.findViewById(R.id.button);
            Content = view.findViewById(R.id.Content);
        }
    }
}