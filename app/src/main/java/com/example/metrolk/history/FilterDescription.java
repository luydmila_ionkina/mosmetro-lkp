package com.example.metrolk.history;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.adapters.MyCardAdapter;

import java.util.Calendar;

public class FilterDescription extends AppCompatActivity {

    Calendar dateAndTime = Calendar.getInstance();
    EditText date;
    RecyclerView CardView, MethodPayment;
    LinearLayout datePayment, typeOperation;
    ImageView imageUp1, imageUp2, imageUp3, imageUp4;

    int up1 = 0, up2 = 0, up3 = 0, up4 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_description);

        Toolbar toolbar = findViewById(R.id.toolbar);

        ImageView back = toolbar.findViewById(R.id.back);
        back.setImageResource(R.drawable.ic_close);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView textBar = findViewById(R.id.textBar);
        textBar.setText(R.string.filter);

        date = findViewById(R.id.date);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        CardView = findViewById(R.id.CardView);
        CardView.setLayoutManager(linearLayoutManager);
        CardView.setAdapter(new MyCardAdapter(this,
                getResources().getDisplayMetrics().density, StaticData.Element.checkTransportCard, 3, null, null));

        StaticData.History type = (StaticData.History) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case trip:
                    break;
                case operation: operation();
                    break;
            }
        }

        datePayment = findViewById(R.id.datePayment);
        imageUp1 = findViewById(R.id.imageUp1);
        imageUp2 = findViewById(R.id.imageUp2);
        imageUp3 = findViewById(R.id.imageUp3);
        imageUp4 = findViewById(R.id.imageUp4);
    }

    public void operation() {
        LinearLayout layoutMethodPayment = findViewById(R.id.layoutMethodPayment);
        layoutMethodPayment.setVisibility(View.VISIBLE);

        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        MethodPayment = findViewById(R.id.MethodPayment);
        MethodPayment.setLayoutManager(linearLayoutManager2);
        MethodPayment.setAdapter(new MyCardAdapter(this,
                getResources().getDisplayMetrics().density, StaticData.Element.checkBankCard, 3, null, null));

        LinearLayout type = findViewById(R.id.type);
        type.setVisibility(View.VISIBLE);

        typeOperation = findViewById(R.id.typeOperation);
        typeOperation.setVisibility(View.VISIBLE);
    }

    // диалоговое окно для выбора даты
    public void onClickDate(View view) {
        date.setTextColor(StaticData.colorBlack);
        new DatePickerDialog(this, d,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    // установка обработчика выбора даты
    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, monthOfYear);
            dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialDateTime();
        }
    };

    // установка начальных даты и времени
    private void setInitialDateTime() {
        date.setText(DateUtils.formatDateTime(this,
                dateAndTime.getTimeInMillis(), DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR));
    }

    public void onClickUp1(View view) {
        if(up1 == 0) {
            MethodPayment.setVisibility(View.GONE);
            imageUp1.setRotation(90);
            up1++;
        }
        else {
            MethodPayment.setVisibility(View.VISIBLE);
            imageUp1.setRotation(270);
            up1 = 0;
        }
    }

    public void onClickUp2(View view) {
        if(up2 == 0) {
            CardView.setVisibility(View.GONE);
            imageUp2.setRotation(90);
            up2++;
        }
        else {
            CardView.setVisibility(View.VISIBLE);
            imageUp2.setRotation(270);
            up2 = 0;
        }
    }

    public void onClickUp3(View view) {
        if(up3 == 0) {
            datePayment.setVisibility(View.GONE);
            imageUp3.setRotation(90);
            up3++;
        }
        else {
            datePayment.setVisibility(View.VISIBLE);
            imageUp3.setRotation(270);
            up3 = 0;
        }
    }

    public void onClickUp4(View view) {
        if(up4 == 0) {
            typeOperation.setVisibility(View.GONE);
            imageUp4.setRotation(90);
            up4++;
        }
        else {
            typeOperation.setVisibility(View.VISIBLE);
            imageUp4.setRotation(270);
            up4 = 0;
        }
    }

    public void onClickApply(View view) {
        finish();
    }
}
