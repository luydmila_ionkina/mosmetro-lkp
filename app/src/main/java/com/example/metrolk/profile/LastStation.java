package com.example.metrolk.profile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.metrolk.R;

public class LastStation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_appeal);

        TextView title = findViewById(R.id.title);
        title.setText(R.string.add_card);

        TextView text = findViewById(R.id.text);
        text.setVisibility(View.VISIBLE);

        View.OnClickListener selectStation = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LastStation.this, SelectStation.class));
            }
        };

        TextView text_1 = findViewById(R.id.text_1);
        text_1.setText(R.string.select_station);

        TextView value_1 = findViewById(R.id.value_1);
        value_1.setText(R.string.example_station);
        value_1.setOnClickListener(selectStation);

        TextView text_2 = findViewById(R.id.text_2);
        text_2.setText(R.string.select_station);

        TextView value_2 = findViewById(R.id.value_2);
        value_2.setOnClickListener(selectStation);

        TextView value_3 = findViewById(R.id.value_3);
        value_3.setOnClickListener(selectStation);

        LinearLayout layout_3 = findViewById(R.id.Layout_3);
        layout_3.setVisibility(View.GONE);

        LinearLayout emptyLayout = findViewById(R.id.EmptyLayout);
        emptyLayout.setVisibility(View.VISIBLE);
    }

    public void onClickNext(View view) {
        startActivity(new Intent(this, CardConnected.class));
    }

    public void onClickBack(View view) {
        finish();
    }
}
