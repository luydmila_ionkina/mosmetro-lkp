package com.example.metrolk.map;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.yandex.mapkit.Animation;
import com.yandex.mapkit.MapKitFactory;
import com.yandex.mapkit.ScreenPoint;
import com.yandex.mapkit.geometry.Point;
import com.yandex.mapkit.logo.Alignment;
import com.yandex.mapkit.logo.HorizontalAlignment;
import com.yandex.mapkit.logo.VerticalAlignment;
import com.yandex.mapkit.map.CameraPosition;
import com.yandex.mapkit.map.MapObject;
import com.yandex.mapkit.map.MapObjectTapListener;
import com.yandex.mapkit.map.PlacemarkMapObject;
import com.yandex.mapkit.mapview.MapView;
import com.yandex.runtime.image.ImageProvider;
import com.yandex.runtime.ui_view.ViewProvider;

public class MapPayments extends AppCompatActivity {

    private MapView mapview;
    BottomSheetDialog dialogBubble = null;

    double latitude[] = {55.757370, 55.760421, 55.757032};
    double longitude[] = {37.620359, 37.626669, 37.599751};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_map_payments);

        mapview = (MapView) findViewById(R.id.mapview);
        mapview.getMap().getLogo().setAlignment(new Alignment(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM));
        mapview.getMap().move(
                new CameraPosition(new Point(55.755814, 37.617635), 11.0f, 0.0f, 0.0f),
                new Animation(Animation.Type.SMOOTH, 0),
                null);

        PlacemarkMapObject placemarkMapObject = mapview.getMap().getMapObjects().addPlacemark(new Point(latitude[0], longitude[0]));
        placemarkMapObject.setUserData(null);
        addPlacemarks();
    }

    public void addPlacemarks() {
        for(int i = 0; i < latitude.length; i++) {
            mapview.getMap()
                    .getMapObjects()
                    //.addPlacemark(new Point(latitude[i], longitude[i]), ImageProvider.fromBitmap(getMarker()))
                    .addPlacemark(new Point(latitude[i], longitude[i]), ImageProvider.fromResource(this, R.drawable.marker))
                    .addTapListener(new MapObjectTapListener() {
                        @Override
                        public boolean onMapObjectTap(@NonNull MapObject mapObject, @NonNull Point point) {
                            if(dialogBubble == null) showDialogBubble();
                            else {
                                dialogBubble.dismiss();
                                showDialogBubble();
                            }
                            return false;
                        }
                    });
        }
    }

    public Bitmap getMarker(){
        View view = getLayoutInflater().inflate(R.layout.marker_terminal_till, null);
        view.buildDrawingCache(true);
        view.setDrawingCacheEnabled(true);
        return view.getDrawingCache();
    }

    private void showDialogBubble() {
        dialogBubble = new BottomSheetDialog(MapPayments.this, R.style.CustomBottomSheetDialogTheme);
        dialogBubble.setCancelable(true);
        dialogBubble.setContentView(R.layout.dialog_bottom_terminal);
        dialogBubble.show();
    }

    public void onClickZoomIn() {
    }

    public void onClickZoomOut() {
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapview.onStop();
        MapKitFactory.getInstance().onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mapview.onStart();
        MapKitFactory.getInstance().onStart();
    }

    public void onClickFilter(View view) {
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_bottom_filter, null);

        final ImageView imageCheck1 = dialogView.findViewById(R.id.check1);
        final ImageView imageCheck2 = dialogView.findViewById(R.id.check2);
        final ImageView imageCheck3 = dialogView.findViewById(R.id.check3);
        final ImageView imageCheck4 = dialogView.findViewById(R.id.check4);
        final ImageView imageCheck5 = dialogView.findViewById(R.id.check5);

        final ImageView image[] = { imageCheck1, imageCheck2, imageCheck3, imageCheck4, imageCheck5 };

        LinearLayout layoutCheck1 = dialogView.findViewById(R.id.LayoutCheck1);
        layoutCheck1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectItem(imageCheck1, image);
            }
        });

        LinearLayout layoutCheck2 = dialogView.findViewById(R.id.LayoutCheck2);
        layoutCheck2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectItem(imageCheck2, image);
            }
        });

        LinearLayout layoutCheck3 = dialogView.findViewById(R.id.LayoutCheck3);
        layoutCheck3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectItem(imageCheck3, image);
            }
        });

        LinearLayout layoutCheck4 = dialogView.findViewById(R.id.LayoutCheck4);
        layoutCheck4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectItem(imageCheck4, image);
            }
        });

        LinearLayout layoutCheck5 = dialogView.findViewById(R.id.LayoutCheck5);
        layoutCheck5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectItem(imageCheck5, image);
            }
        });

        BottomSheetDialog dialog = new BottomSheetDialog(this, R.style.CustomBottomSheetDialogTheme);
        dialog.setCancelable(true);
        dialog.setContentView(dialogView);
        dialog.show();
    }

    public void selectItem(ImageView imageCheck, ImageView imageNotChecked[]) {
        for(int i = 0; i < imageNotChecked.length; i++)
            imageNotChecked[i].setVisibility(View.INVISIBLE);

        imageCheck.setVisibility(View.VISIBLE);
    }

    public void onClickListTerminal(View view) {
        Intent intent = new Intent(this, MapSearch.class);
        intent.putExtra("type", StaticData.Map.terminal);
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }
}
