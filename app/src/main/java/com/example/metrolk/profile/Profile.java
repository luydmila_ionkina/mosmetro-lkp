package com.example.metrolk.profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.metrolk.MainActivity;
import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.autorization.AutorizationActivity;
import com.example.metrolk.autorization.EnterCode;
import com.example.metrolk.recovery.MethodRecovery;
import com.example.metrolk.registration.CreateUserCode;

public class Profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
    }

    public void onChangeCode(View view) {
        Intent intent = new Intent(this, EnterCode.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("type", StaticData.ActivityType.changeCode);
        startActivity(intent);
    }

    public void onClickNotic(View view) {
        startActivity(new Intent(this, SettingsNotification.class));
    }

    public void onClickSecurity(View view) {
        Intent intent = new Intent(this, MethodRecovery.class);
        intent.putExtra("type", StaticData.ActivityType.security);
        startActivity(intent);
    }

    public void onClickExit(View view) {
        View.OnClickListener buttonClick2 = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = MainActivity.mSettings.edit();
                editor.putString(MainActivity.AUTORIZATION, "notAuthorized");
                editor.apply();

                Intent intent = new Intent(Profile.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("type", StaticData.ActivityType.notAuthorized);
                startActivity(intent);
            }
        };

        StaticData.showDialogTwoButton(getString(R.string.exit_lk_2), getString(R.string.exit_confirmation),
                getString(R.string.cancel), buttonClick2, getString(R.string.exit_2), this);
    }

    public void onClickBack(View view) {
        SharedPreferences.Editor editor = MainActivity.mSettings.edit();
        editor.putString(MainActivity.AUTORIZATION, "authorized");
        editor.apply();

        Intent intent = new Intent(Profile.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("type", StaticData.ActivityType.authorized);
        startActivity(intent);
    }
}
