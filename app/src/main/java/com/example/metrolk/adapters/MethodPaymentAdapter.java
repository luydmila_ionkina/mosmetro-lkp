package com.example.metrolk.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.payment.CardDescription;
import com.makeramen.roundedimageview.RoundedImageView;

public class MethodPaymentAdapter extends RecyclerView.Adapter<MethodPaymentAdapter.ViewHolder> {

    private final Context mContext;
    private LayoutInflater inflater;
    private String cardName[] = { "Моя карта", "MasterCard", "МИР", "UnionPay", "American Express", "JCB" };
    private int cardLogo[] = { R.drawable.ic_visa_logo, R.drawable.ic_master_card_logo, R.drawable.ic_mir_logo,
            R.drawable.ic_union_pay_logo, R.drawable.ic_american_express_logo, R.drawable.ic_jcb_logo};
    private int colorCard[] = { StaticData.colorVisa, StaticData.colorMasterCard, StaticData.colorMir,
            StaticData.colorUnionPay, StaticData.colorAmericanExpress, StaticData.colorJCB };

   /* private int cardImage[] = {R.drawable.ic_visa_small, R.drawable.ic_master_card, R.drawable.ic_mir,
            R.drawable.ic_union_pay, R.drawable.ic_american_express, R.drawable.ic_jcb};*/

    public MethodPaymentAdapter(Context c) {
        this.mContext = c;
        this.inflater = LayoutInflater.from(mContext);
    }

    @Override
    public MethodPaymentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_details, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MethodPaymentAdapter.ViewHolder holder, final int position) {
        float density = mContext.getResources().getDisplayMetrics().density;

        holder.name.setText(cardName[position]);
        //holder.card.setImageResource(cardImage[position]);
        holder.card.setImageResource(cardLogo[position]);
        holder.card.setBackgroundColor(colorCard[position]);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.height = (int) (32 * density + 0.5f);
        layoutParams.width = (int) (32 * density + 0.5f);
        holder.card.setLayoutParams(layoutParams);
        holder.card.setPadding((int) (4 * density + 0.5f), (int) (4 * density + 0.5f), (int) (4 * density + 0.5f), (int) (4 * density + 0.5f));
        holder.card.setCornerRadius((int) (6.4 * density + 0.5f));

        holder.layoutCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CardDescription.class);
                intent.putExtra("name", cardName[position]);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cardName.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        RoundedImageView card;
        LinearLayout layoutCard;

        ViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.name);
            card = view.findViewById(R.id.card);
            layoutCard = view.findViewById(R.id.layoutCard);
        }
    }
}