package com.example.metrolk.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;

public class ControlQuestions extends AppCompatActivity {

    StaticData.ActivityType type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_questions);

        type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case controlQuestion:
                    TextView title = findViewById(R.id.title);
                    title.setText(R.string.control_question);
                    break;
            }
        }
    }

    public void onClickQuestion1(View view) {
        UserQuestion(getString(R.string.question_1));
    }

    public void onClickQuestion2(View view) {
        UserQuestion(getString(R.string.question_2));
    }

    public void onClickQuestion3(View view) {
        UserQuestion(getString(R.string.question_3));
    }

    public void onClickQuestion4(View view) {
        UserQuestion(getString(R.string.question_4));
    }

    public void onClickQuestion5(View view) {
        UserQuestion(getString(R.string.question_5));
    }

    public void UserQuestion(String question) {
        Intent intent = new Intent(this, UserQuestion.class);
        intent.putExtra("question", question);

        if(type != null) {
            switch(type) {
                case controlQuestion:  intent.putExtra("type", StaticData.ActivityType.controlQuestion);
                    break;
            }
        }
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }
}
