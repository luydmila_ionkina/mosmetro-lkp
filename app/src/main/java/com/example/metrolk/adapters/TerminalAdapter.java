package com.example.metrolk.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;
import com.example.metrolk.payment.WrittenSumMetro;

public class TerminalAdapter extends RecyclerView.Adapter<TerminalAdapter.ViewHolder> {

    private final Context mContext;
    private LayoutInflater inflater;
    private StaticData.Terminal type;
    public int colors[] = { StaticData.colorGrayText };

    public TerminalAdapter(Context c, StaticData.Terminal t) {
        this.mContext = c;
        this.inflater = LayoutInflater.from(mContext);
        type = t;
    }

    @Override
    public TerminalAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.terminal_search, parent, false);

        if(type != null) {
            switch(type) {
                case searchTerminal: view = inflater.inflate(R.layout.terminal_search, parent, false);
                    break;
                case imageTerminal: view = inflater.inflate(R.layout.terminal_image, parent, false);
                    break;
            }
        }
        return new ViewHolder(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(TerminalAdapter.ViewHolder holder, final int position) {
        if(type != null) {
            switch(type) {
                case searchTerminal:
                    break;
                case imageTerminal:
                    holder.image.setBackgroundColor(colors[0]);
            }
        }
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RoundButton image;

        ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
        }
    }
}