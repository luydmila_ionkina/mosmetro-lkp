package com.example.metrolk.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.PhoneEditText;
import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.profile.CardConnected;

public class CreateUserAccount extends AppCompatActivity {

    PhoneEditText enter_tel;
    Button buttonNext;
    StaticData.ActivityType type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization);

        findViewById(R.id.enter_tel).setVisibility(View.VISIBLE);
        findViewById(R.id.enter_tel).requestFocus();

        ImageView close = findViewById(R.id.close);
        close.setImageResource(R.drawable.ic_back);

        ImageView question = findViewById(R.id.question);
        question.setEnabled(true);
        question.setVisibility(View.INVISIBLE);

        TextView title = findViewById(R.id.title);
        TextView text = findViewById(R.id.text);

        LinearLayout content = findViewById(R.id.Content);
        content.setVisibility(View.GONE);

        EditText passwordText = findViewById(R.id.enter);
        TextView recovery = findViewById(R.id.recovery);
        recovery.setVisibility(View.INVISIBLE);
        recovery.setEnabled(false);

        passwordText.setVisibility(View.GONE);

        enter_tel = findViewById(R.id.enter_tel);
        enter_tel.setContext(this);

        buttonNext = findViewById(R.id.buttonNext);

        type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case socialNetwork:
                case createUserAccount:
                    title.setText(R.string.create_user_account);
                    text.setText(R.string.enter_code_7);
                    break;
                case recoveryLogin:
                    title.setText(R.string.recovery_state);
                    text.setText(R.string.enter_code_11);
                    break;
                case addCardConnectedPhone:
                    title.setText(R.string.add_card);
                    text.setText(R.string.enter_mobile_tel);
                    break;
                case changePhone:
                    title.setText(R.string.security_lk);
                    text.setText(R.string.enter_code_11);
                    break;
            }
        }

    }

    public void onClickNext(View view) {
        if(type != null) {
            Intent intent;

            switch(type) {
                case socialNetwork:
                case createUserAccount:
                case recoveryLogin:
                case changePhone:
                    intent = new Intent(this, CreateUserCode.class);
                    intent.putExtra("number", enter_tel.getText().toString());
                    intent.putExtra("type", type);
                    startActivity(intent);
                    break;
                case addCardConnectedPhone:
                    startActivity(new Intent(this, CardConnected.class));
                    break;
            }
        }

    }

    public void onClickBack(View view) {
        finish();
    }
}
