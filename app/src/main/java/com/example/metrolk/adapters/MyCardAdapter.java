package com.example.metrolk.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.payment.AddCard;
import com.example.metrolk.payment.AddCardScore;
import com.example.metrolk.profile.MMCardDescription;
import com.example.metrolk.recovery.MethodRecovery;
import com.example.metrolk.tariff.Tariff;
import com.makeramen.roundedimageview.RoundedImageView;

public class MyCardAdapter extends RecyclerView.Adapter<MyCardAdapter.ViewHolder> {

    private final Context mContext;
    private LayoutInflater inflater;
    private float density;
    private StaticData.Element element;
    private int count;
    private RoundedImageView imageCard;
    private BottomSheetDialog dialog;

    private String nameTransportCard[] = { "Моя тройка", "Любимая", "Карта москвича" };
    private String numberTransportCard[] = { "1234 567 898", "**** 1234 08/21", "Ожидает привязки" };
    private int backgroundTransportCard[] = { R.drawable.background_card_1, R.drawable.background_card_2, R.drawable.background_card_3,
            R.drawable.background_card_4, R.drawable.background_card_5, R.drawable.background_card_6 };

    private StaticData.CardType cardType[] = { StaticData.CardType.visa, StaticData.CardType.masterCard, StaticData.CardType.mir };
    private String nameBankCard[] = { "Моя карта", "MasterCard", "МИР" };
    private String numberBankCard[] = { "**** 1234 08/20", "**** 1234 08/20", "**** 1234 08/20" };

    private int cardLogo[] = { R.drawable.ic_visa_logo, R.drawable.ic_master_card_logo, R.drawable.ic_mir_logo,
            R.drawable.ic_union_pay_logo, R.drawable.ic_american_express_logo, R.drawable.ic_jcb_logo};
    private int colorCard[] = { StaticData.colorVisa, StaticData.colorMasterCard, StaticData.colorMir,
            StaticData.colorUnionPay, StaticData.colorAmericanExpress, StaticData.colorJCB };

    //private int backgroundBankCard[] = { R.drawable.ic_visa_small, R.drawable.ic_master_card, R.drawable.ic_mir };

    public MyCardAdapter(Context context, float d, StaticData.Element el, int c, RoundedImageView image, BottomSheetDialog dialog) {
        this.mContext = context;
        this.density = d;
        this.inflater = LayoutInflater.from(mContext);
        this.element = el;
        this.count = c;
        this.imageCard = image;
        this.dialog = dialog;
    }

    @Override
    public MyCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch(element) {
            case panelCards: view = inflater.inflate(R.layout.card_small, parent, false);
                break;
            case myCards: view = inflater.inflate(R.layout.card_large, parent, false);
                break;
            case additionScore: view = inflater.inflate(R.layout.addition_score, parent, false);
                break;
            case checkTransportCard:
            case checkBankCard:
                view = inflater.inflate(R.layout.check_card, parent, false);
                ImageView imageNext = view.findViewById(R.id.imageNext);
                imageNext.setVisibility(View.INVISIBLE);
                break;
            case bankCard:
            case transportCard:
                view = inflater.inflate(R.layout.card_details, parent, false);
                ImageView imageNext2 = view.findViewById(R.id.imageNext);
                imageNext2.setVisibility(View.INVISIBLE);
                break;
            default: view = inflater.inflate(R.layout.card_large, parent, false);
        }

        return new MyCardAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyCardAdapter.ViewHolder holder, final int position) {
        switch(element) {
            case panelCards:
                if (position == 0) {
                    holder.layoutCart.setBackgroundResource(R.drawable.round_message_red);
                    FrameLayout.LayoutParams layoutParam = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
                    layoutParam.height = (int) (128 * density + 0.5f);
                    layoutParam.width = (int) (72 * density + 0.5f);
                    holder.layoutCart.setLayoutParams(layoutParam);

                    holder.layoutCart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(mContext, AddCard.class);
                            intent.putExtra("type", StaticData.ActivityType.addTransportCard);
                            mContext.startActivity(intent);
                        }
                    });

                    ImageView addCart = new ImageView(mContext);
                    addCart.setImageResource(R.drawable.ic_add_cart);

                    holder.layoutCart.removeAllViews();
                    holder.layoutCart.addView(addCart);
                } else showMyCards(holder, position-1);
                break;
            case myCards:
            case additionScore:
            case checkTransportCard:
            case checkBankCard:
            case bankCard:
            case transportCard: showMyCards(holder, position);
                break;
        }
}

    public void showMyCards(final MyCardAdapter.ViewHolder holder, final int position) {
        switch(element) {
            case myCards:
            case panelCards:
                if(position == 2) {
                    animation(holder.layout1);
                    animation(holder.layout2);
                }
                holder.name.setText(nameTransportCard[position]);
                holder.background.setBackgroundResource(backgroundTransportCard[position]);

                holder.layoutCart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, MMCardDescription.class);
                        intent.putExtra("name", nameTransportCard[position]);
                        intent.putExtra("background", backgroundTransportCard[position]);
                        if(position == 1) intent.putExtra("type", StaticData.Card.social);
                        else intent.putExtra("type", StaticData.Card.limited);
                        mContext.startActivity(intent);
                    }
                });
                break;
            case additionScore:
                holder.layoutCart.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, MethodRecovery.class);
                        intent.putExtra("type", StaticData.ActivityType.writtenScore);
                        mContext.startActivity(intent);
                    }
                });
                break;
            case checkTransportCard:
                holder.name.setText(nameTransportCard[position]);
                holder.roundCard.setBackgroundResource(backgroundTransportCard[position]);
                if(numberTransportCard[position].contains("Ожидает")) holder.layoutCard.setBackgroundColor(StaticData.colorGray);
                holder.number.setText(numberTransportCard[position]);
                break;
            case checkBankCard:
                holder.name.setText(nameBankCard[position]);
                holder.roundCard.setImageResource(cardLogo[position]);
                holder.roundCard.setBackgroundColor(colorCard[position]);
                holder.roundCard.setCornerRadius((int) (6.4 * density + 0.5f));
                holder.card.setPadding((int) (4 * density + 0.5f), (int) (4 * density + 0.5f), (int) (4 * density + 0.5f), (int) (4 * density + 0.5f));
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.height = (int) (32 * mContext.getResources().getDisplayMetrics().density + 0.5f);
                layoutParams.width = (int) (32 * mContext.getResources().getDisplayMetrics().density + 0.5f);
                holder.roundCard.setLayoutParams(layoutParams);
                holder.number.setText(numberBankCard[position]);
                break;
            case bankCard:
                holder.name.setText(nameBankCard[position]);
                holder.roundCard.setImageResource(cardLogo[position]);
                holder.roundCard.setBackgroundColor(colorCard[position]);
                holder.roundCard.setCornerRadius((int) (6.4 * density + 0.5f));
                holder.card.setPadding((int) (4 * density + 0.5f), (int) (4 * density + 0.5f), (int) (4 * density + 0.5f), (int) (4 * density + 0.5f));
                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams2.height = (int) (32 * mContext.getResources().getDisplayMetrics().density + 0.5f);
                layoutParams2.width = (int) (32 * mContext.getResources().getDisplayMetrics().density + 0.5f);
                holder.card.setLayoutParams(layoutParams2);
                holder.number.setText(numberBankCard[position]);
                holder.layoutCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mContext.getClass() == AddCardScore.class) {
                            AddCardScore addCardScore = (AddCardScore) mContext;
                            addCardScore.selectCard2(nameBankCard[position], cardLogo[position], colorCard[position]);
                        }
                        if(mContext.getClass() == Tariff.class) selectedCard(cardType[position]);
                    }
                });
                break;
            case transportCard:
                holder.name.setText(nameTransportCard[position]);
                holder.roundCard.setBackgroundResource(backgroundTransportCard[position]);
                if(numberTransportCard[position].contains("Ожидает")) holder.layoutCard.setBackgroundColor(StaticData.colorGray);
                holder.number.setText(numberTransportCard[position]);
                holder.layoutCard.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mContext.getClass() == AddCardScore.class) {
                            AddCardScore addCardScore = (AddCardScore) mContext;
                            addCardScore.selectCard1(nameTransportCard[position], backgroundTransportCard[position]);
                        }
                    }
                });
                break;
        }
    }

    public void selectedCard(StaticData.CardType type) {
        if(imageCard != null) {
            if(type != null) {
                switch(type) {
                    case visa:
                        imageCard.setImageResource(R.drawable.ic_visa_logo);
                        imageCard.setBackgroundColor(StaticData.colorVisa);
                        break;
                    case masterCard:
                        imageCard.setImageResource(R.drawable.ic_master_card_logo);
                        imageCard.setBackgroundColor(StaticData.colorMasterCard);
                        break;
                    case mir:
                        imageCard.setImageResource(R.drawable.ic_mir_logo);
                        imageCard.setBackgroundColor(StaticData.colorMir);
                        break;
                    case unionPay:
                        imageCard.setImageResource(R.drawable.ic_union_pay_logo);
                        imageCard.setBackgroundColor(StaticData.colorUnionPay);
                        break;
                    case americanExpress:
                        imageCard.setImageResource(R.drawable.ic_american_express_logo);
                        imageCard.setBackgroundColor(StaticData.colorAmericanExpress);
                        break;
                    case jcb:
                        imageCard.setImageResource(R.drawable.ic_jcb_logo);
                        imageCard.setBackgroundColor(StaticData.colorJCB);
                        break;
                }
                if(dialog != null) dialog.dismiss();
            }
        }

    }

    public void animation(final LinearLayout layout) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{ Thread.sleep(5000); }
                catch (Exception e) {}

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(layout.getVisibility() == View.VISIBLE)
                            layout.setVisibility(View.INVISIBLE);
                        else layout.setVisibility(View.VISIBLE);
                        animation(layout);
                    }
                });
            }
        }).start();
    }

    @Override
    public int getItemCount() {
        return count;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout layoutCart;
        LinearLayout cart, layout1, layout2, layoutCard;
        TextView name, number;
        ImageView card, image;
        RoundedImageView background, roundCard;
        CardView cardView;

        ViewHolder(View view) {
            super(view);
            layoutCart = view.findViewById(R.id.LayoutCart);
            cart = view.findViewById(R.id.Cart);
            layout1 = view.findViewById(R.id.layout_1);
            layout2 = view.findViewById(R.id.layout_2);
            name = view.findViewById(R.id.name);
            number = view.findViewById(R.id.number);
            card = view.findViewById(R.id.card);
            image = view.findViewById(R.id.image);
            layoutCard = view.findViewById(R.id.layoutCard);
            background = view.findViewById(R.id.background);
            roundCard = view.findViewById(R.id.card);
            cardView = view.findViewById(R.id.CardView);
        }
    }
}