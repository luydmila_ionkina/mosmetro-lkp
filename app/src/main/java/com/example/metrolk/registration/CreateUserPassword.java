package com.example.metrolk.registration;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.MainActivity;
import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.autorization.AutorizationActivity;
import com.example.metrolk.autorization.EnterCode;
import com.example.metrolk.profile.Profile;

public class CreateUserPassword extends AppCompatActivity {

    EditText passwordText;
    TextView text;
    String passwordFirst = null, passwordSecond = null;
    TextView passwordHint;
    StaticData.ActivityType type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization);

        findViewById(R.id.enter).requestFocus();

        ImageView close = findViewById(R.id.close);
        close.setImageResource(R.drawable.ic_back);

        ImageView question = findViewById(R.id.question);
        question.setEnabled(true);
        question.setVisibility(View.INVISIBLE);

        TextView title = findViewById(R.id.title);
        title.setText(R.string.create_user_account);

        LinearLayout content = findViewById(R.id.Content);
        content.setVisibility(View.GONE);

        text = findViewById(R.id.text);
        text.setText(R.string.enter_code_9);

        TextView recovery = findViewById(R.id.recovery);
        recovery.setVisibility(View.INVISIBLE);
        recovery.setEnabled(false);

        passwordText = findViewById(R.id.enter);
        passwordText.setHint(R.string.password);
        passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        final TextView loginHint = findViewById(R.id.loginHint);
        loginHint.setText(R.string.password);

        passwordHint = findViewById(R.id.passwordHint);

        final Button buttonNext = findViewById(R.id.buttonNext);
        final LinearLayout divider = findViewById(R.id.divider);

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (passwordText.length() > 0) {
                    loginHint.setVisibility(View.VISIBLE);
                    divider.setBackgroundColor(StaticData.colorRed);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (2 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                }
                if (passwordText.length() > 6) {
                    buttonNext.setEnabled(true);
                    buttonNext.setTextColor(StaticData.colorWhite);
                    buttonNext.setBackgroundResource(R.drawable.round_message_red);
                }
                if (passwordText.length() <= 6) {
                    buttonNext.setEnabled(false);
                    buttonNext.setTextColor(StaticData.colorGrayText);
                    buttonNext.setBackgroundResource(R.drawable.round_gray);
                }
                if (passwordText.length() <= 0) {
                    loginHint.setVisibility(View.INVISIBLE);
                    divider.setBackgroundColor(StaticData.colorGray);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (1 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        passwordText.addTextChangedListener(watcher);

        type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case recoveryPassword:
                    title.setText(R.string.create_password);
                    text.setText(R.string.enter_code_12);
                    break;
                case controlQuestion:
                    title.setText(R.string.security_lk);
                    text.setText(R.string.enter_pass_to_change_contr_q);

                    buttonNext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(CreateUserPassword.this, ControlQuestions.class);
                            intent.putExtra("type", StaticData.ActivityType.controlQuestion);
                            startActivity(intent);
                        }
                    });
                    break;
                case reservMethod:
                    title.setText(R.string.security_lk);
                    text.setText(R.string.enter_pass_to_change_r_method);

                    buttonNext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(CreateUserPassword.this, ReservRecovery.class);
                            intent.putExtra("type", StaticData.ActivityType.reservMethod);
                            startActivity(intent);
                        }
                    });
                    break;
                case changePassword:
                    title.setText(R.string.security_lk);
                    text.setText(R.string.enter_pass_to_change_pass);

                    buttonNext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(CreateUserPassword.this, CreateUserPassword.class);
                            intent.putExtra("type", StaticData.ActivityType.newPassword);
                            startActivity(intent);
                        }
                    });
                    break;
                case changePhone:
                    title.setText(R.string.security_lk);
                    text.setText(R.string.enter_pass_to_change_phone);

                    buttonNext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(CreateUserPassword.this, CreateUserAccount.class);
                            intent.putExtra("type", type);
                            startActivity(intent);
                        }
                    });
                    break;
                case changeEmail:
                    title.setText(R.string.security_lk);
                    text.setText(R.string.enter_pass_to_change_email);

                    buttonNext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(CreateUserPassword.this, ReservRecovery.class);
                            intent.putExtra("type", type);
                            startActivity(intent);
                        }
                    });
                    break;
                case newPassword:
                    title.setText(R.string.security_lk);
                    text.setText(R.string.enter_pass_to_change_pass);

                    text.setText(R.string.enter_code_13);

                    buttonNext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onChangePassword();
                        }
                    });
                    break;
            }
        }
    }

    public void onChangePassword() {
        passwordHint.setVisibility(View.INVISIBLE);

        if (passwordFirst == null) {
            text.setText(R.string.enter_code_10);
            passwordFirst = passwordText.getText().toString();
            passwordText.setText("");
        } else {
            passwordSecond = passwordText.getText().toString();
            if (passwordFirst.equals(passwordSecond)) {

                if(type != null) {
                    switch(type) {
                        case recoveryPassword:
                            View.OnClickListener buttonClick = new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(CreateUserPassword.this, AutorizationActivity.class));
                                }
                            };

                            StaticData.showDialog(getString(R.string.password_change), getString(R.string.password_change_2),
                                    getString(R.string.show_autorization), buttonClick, this);
                            break;
                        case controlQuestion:
                            break;
                        case reservMethod:
                            break;
                        case changePassword:
                            startActivity(new Intent(this, ControlQuestions.class));
                            break;
                        case newPassword: showLKP();
                            break;
                    }
                } else {
                    Intent intent = new Intent(CreateUserPassword.this, EnterCode.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("type", StaticData.ActivityType.newCode);
                    startActivity(intent);
                } //startActivity(new Intent(this, ControlQuestions.class));
            } else {
                passwordHint.setText(R.string.password_not_equal);
                passwordHint.setVisibility(View.VISIBLE);

                passwordFirst = null;
                passwordSecond = null;
                text.setText(R.string.enter_code_9);
                passwordText.setText("");
            }
        }
    }

    public void showLKP() {
        View.OnClickListener buttonClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = MainActivity.mSettings.edit();
                editor.putString(MainActivity.AUTORIZATION, "authorized");
                editor.apply();

                Intent intent = new Intent(CreateUserPassword.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.putExtra("type", StaticData.ActivityType.authorized);
                startActivity(intent);
            }
        };

        StaticData.showDialog(getString(R.string.password_change), getString(R.string.password_change_2),
                getString(R.string.show_personal_account), buttonClick, this);
    }

    public void onClickNext(View view) {
        onChangePassword();
    }

    public void onClickBack(View view) {
        finish();
    }
}
