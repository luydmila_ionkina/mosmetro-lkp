package com.example.metrolk;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;
import com.example.metrolk.adapters.MessageAdapter;
import com.example.metrolk.adapters.MyCardAdapter;
import com.example.metrolk.autorization.AutorizationActivity;
import com.example.metrolk.autorization.EnterCode;
import com.example.metrolk.communication.Communication;
import com.example.metrolk.history.FilterOperation;
import com.example.metrolk.map.MapPayments;
import com.example.metrolk.payment.AddCardScore;
import com.example.metrolk.payment.MethodPayment;
import com.example.metrolk.profile.MessagePanel;
import com.example.metrolk.profile.MyCard;
import com.example.metrolk.profile.Profile;
import com.example.metrolk.registration.ReservRecovery;
import com.example.metrolk.tariff.Tariff;
import com.yandex.mapkit.MapKitFactory;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    public static SharedPreferences mSettings;
    public static final String PREFERENCES = "mysettings";
    public static final String AUTORIZATION = "notAuthorized";
    public static String ANDROID_ID = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        MapKitFactory.setApiKey("dee693d0-349d-4757-9a94-e9a237abb930");
        MapKitFactory.initialize(this);

        ANDROID_ID = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        mSettings = getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        Log.d(" - - - - - token", mSettings.getString(AUTORIZATION, "null"));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.parseColor("#000000"));
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView icon = toolbar.findViewById(R.id.icon);
        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Profile.class));
            }
        });

        RelativeLayout recoveryLayout = findViewById(R.id.recovery);
        if(recoveryLayout != null) {
            recoveryLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*Intent intent = new Intent(MainActivity.this, ReservRecovery.class);
                    intent.putExtra("type", StaticData.ActivityType.reservRecovery);
                    startActivity(intent);*/
                    Intent intent = new Intent(MainActivity.this, EnterCode.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("type", StaticData.ActivityType.changeCode);
                    startActivity(intent);
                }
            });
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView MessageView = findViewById(R.id.MessageView);
        MessageView.setLayoutManager(linearLayoutManager);
        MessageView.setAdapter(new MessageAdapter(this));

        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        final RecyclerView CartsView = findViewById(R.id.CartsView);
        CartsView.setLayoutManager(linearLayoutManager2);
        CartsView.setAdapter(new MyCardAdapter(this, getResources().getDisplayMetrics().density,
                StaticData.Element.panelCards, 4, null, null));

        //StaticData.ActivityType type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");

        if(mSettings.contains(AUTORIZATION) & mSettings.getString(AUTORIZATION, "null") != null) {
            if(mSettings.getString(MainActivity.AUTORIZATION, "null").equals("authorized")) {
                final RelativeLayout recovery = findViewById(R.id.recovery);

                ImageView close = findViewById(R.id.close);
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        recovery.setVisibility(View.GONE);
                    }
                });
            }
            else {
                authorization(icon);
            }
        } else authorization(icon);
    }

    public void authorization(ImageView icon) {
        LinearLayout layoutAutorization = findViewById(R.id.Autorization);
        layoutAutorization.setVisibility(View.VISIBLE);
        layoutAutorization.setEnabled(true);

        LinearLayout layoutDialog = findViewById(R.id.autorizationDialog);
        layoutDialog.setVisibility(View.VISIBLE);

        RelativeLayout recovery = findViewById(R.id.recovery);
        recovery.setVisibility(View.GONE);

        icon.setVisibility(View.INVISIBLE);

        LinearLayout layoutPayments = findViewById(R.id.Payments);
        layoutPayments.setVisibility(View.GONE);

        LinearLayout layoutHistoryOperation = findViewById(R.id.HistoryOperation);
        layoutHistoryOperation.setVisibility(View.GONE);

        LinearLayout layoutHistoryTravel = findViewById(R.id.HistoryTravel);
        layoutHistoryTravel.setVisibility(View.GONE);

        LinearLayout layoutContact = findViewById(R.id.Contact);
        layoutContact.setVisibility(View.GONE);
    }

    public void onClickMethodPayment(View view){
        startActivity(new Intent(this, MethodPayment.class));
    }

    public void onClickAutorization(View view) {
        startActivity(new Intent(this, AutorizationActivity.class));
    }

    public void onClickMyCard(View view) {
        startActivity(new Intent(this, MyCard.class));
    }

    public void onClickMessagePanel(View view) {
        startActivity(new Intent(this, MessagePanel.class));
    }

    public void onClickCommunication(View view) {
        //startActivity(new Intent(this, Communication.class));
    }

    public void onClickTariff(View view) {
        Intent intent = new Intent(this, Tariff.class);
        intent.putExtra("type", StaticData.ActivityType.ticketCard);
        startActivity(intent);
    }

    public void onClickAddCardScore(View view) {
        startActivity(new Intent(this, AddCardScore.class));
    }

    public void onClickMap(View view) {
        startActivity(new Intent(this, MapPayments.class));
    }

    public void onClickHistoryTrip(View view) {
        Intent intent = new Intent(this, FilterOperation.class);
        intent.putExtra("type", StaticData.History.trip);
        startActivity(intent);
    }

    public void onClickHistoryOperation(View view) {
        Intent intent = new Intent(this, FilterOperation.class);
        intent.putExtra("type", StaticData.History.operation);
        startActivity(intent);
    }
}
