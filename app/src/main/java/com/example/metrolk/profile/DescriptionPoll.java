package com.example.metrolk.profile;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;

public class DescriptionPoll extends AppCompatActivity {

    int allQuestion, question, remains;
    TextView countQuestion, countQStay;
    String value[] = {"Утром", "Днем", "Вечером", ""};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description_poll);

        Toolbar toolbar = findViewById(R.id.toolbar);

        ImageView back = toolbar.findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView textBar = findViewById(R.id.textBar);
        textBar.setText("Опрос");

        allQuestion = 5;
        question = 1;
        remains = allQuestion - 1;

        TextView titleView = findViewById(R.id.title);

        String title = getIntent().getStringExtra("title");
        if(title != null) titleView.setText(title);

        createChecklist();
    }

    public void createChecklist() {
        View view = getLayoutInflater().inflate(R.layout.card_question, null);
        LinearLayout layoutQuestion = findViewById(R.id.layoutQuestion);
        layoutQuestion.addView(view);

        countQuestion = view.findViewById(R.id.countQuestion);
        countQStay = view.findViewById(R.id.countQStay);

        countQuestion.setText(String.valueOf(question));
        countQStay.setText(String.valueOf(remains));

        LinearLayout layoutSwitch = view.findViewById(R.id.layoutSwitch);

        StaticData.Checklist checklist = (StaticData.Checklist) getIntent().getSerializableExtra("checklist");
        if(checklist != null) {
            switch(checklist) {
                case checklist: checklist(layoutSwitch);
                    break;
                case radioGroup: radioGroup(layoutSwitch);
                    break;
            }
        }
    }

    public void checklist(LinearLayout layoutSwitch) {
        for(int i = 0; i < value.length; i++) {
            CheckBox checkBox = new CheckBox(this);
            checkBox.setText(value[i]);
            checkBox.setTextColor(StaticData.colorBlack);
            checkBox.setTextSize(16);
            checkBox.setTypeface(null, Typeface.NORMAL);

            if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.O)
                checkBox.setTypeface(getResources().getFont(R.font.roboto_regular));
            else if(android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.O)
                checkBox.setTypeface(ResourcesCompat.getFont(this, R.font.roboto_regular));

            if(i == value.length - 1) userAnswerChecklist(checkBox, layoutSwitch);
            else layoutSwitch.addView(checkBox);
        }
    }

    public void userAnswerChecklist(CheckBox checkBox, LinearLayout layoutSwitch) {
        EditText editText = new EditText(this);
        editText.setHint("Свой вариант");
        editText.setTextColor(StaticData.colorBlack);
        editText.setBackgroundColor(Color.TRANSPARENT);
        editText.setTextSize(16);
        editText.setTypeface(null, Typeface.NORMAL);

        if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.O)
            editText.setTypeface(getResources().getFont(R.font.roboto_regular));
        else if(android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.O)
            editText.setTypeface(ResourcesCompat.getFont(this, R.font.roboto_regular));

        LinearLayout container = new LinearLayout(this);
        container.setOrientation(LinearLayout.HORIZONTAL);
        container.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        container.addView(checkBox);
        container.addView(editText);

        layoutSwitch.addView(container);
    }

    public void radioGroup(LinearLayout layoutSwitch) {
        RadioButton[] radioButton = new RadioButton[5];
        RadioGroup group = new RadioGroup(this);
        group.setOrientation(RadioGroup.VERTICAL);

        for(int i = 0; i < value.length; i++){
            radioButton[i] = new RadioButton(this);
            radioButton[i].setText(value[i]);
            radioButton[i].setTextColor(StaticData.colorBlack);
            radioButton[i].setTextSize(16);
            radioButton[i].setTypeface(null, Typeface.NORMAL);

            if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.O)
                radioButton[i].setTypeface(getResources().getFont(R.font.roboto_regular));
            else if(android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.O)
                radioButton[i].setTypeface(ResourcesCompat.getFont(this, R.font.roboto_regular));

            if(i == value.length - 1) group.addView(userAnswerRadioGroup(radioButton[i]));
            else group.addView(radioButton[i]);
        }
        layoutSwitch.addView(group);
    }

    public View userAnswerRadioGroup(RadioButton radioButton) {
        EditText editText = new EditText(this);
        editText.setHint("Свой вариант");
        editText.setBackgroundColor(Color.TRANSPARENT);
        editText.setTextColor(StaticData.colorBlack);
        editText.setTextSize(16);
        editText.setTypeface(null, Typeface.NORMAL);

        if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.O)
            editText.setTypeface(getResources().getFont(R.font.roboto_regular));
        else if(android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.O)
            editText.setTypeface(ResourcesCompat.getFont(this, R.font.roboto_regular));

        LinearLayout container = new LinearLayout(this);
        container.setOrientation(LinearLayout.HORIZONTAL);
        container.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        container.addView(radioButton);
        container.addView(editText);

        return container;
    }

    public void onClickNext(View v) {
        if(remains <= 1) {
            final RoundButton button = findViewById(R.id.next);
            button.setButtonText("Отправить результат");
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StaticData.showDialog("Опрос", "Ваш опрос успешно отправлен.", "Закрыть", null, DescriptionPoll.this);

                    LinearLayout layoutQuestion = findViewById(R.id.layoutQuestion);
                    layoutQuestion.setVisibility(View.GONE);

                    button.setButtonText("Опрос пройден");
                    button.setTextColor(StaticData.colorGrayText);
                    button.setBackgroundColor(StaticData.colorGray);
                    button.setEnabled(false);
                }
            });
        }
        else {
            LinearLayout layoutQuestion = findViewById(R.id.layoutQuestion);
            layoutQuestion.removeAllViews();

            question++;
            remains--;

            countQuestion.setText(String.valueOf(question));
            countQStay.setText(String.valueOf(remains));

            createChecklist();
        }
    }
}
