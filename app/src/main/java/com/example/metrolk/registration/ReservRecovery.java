package com.example.metrolk.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;

public class ReservRecovery extends AppCompatActivity {

    EditText passwordText;
    StaticData.ActivityType type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization);

        findViewById(R.id.enter).requestFocus();

        ImageView close = findViewById(R.id.close);
        close.setEnabled(false);
        close.setVisibility(View.INVISIBLE);

        ImageView question = findViewById(R.id.question);
        question.setEnabled(false);
        question.setVisibility(View.INVISIBLE);

        TextView title = findViewById(R.id.title);
        title.setText(R.string.reserv_recovery);

        TextView text = findViewById(R.id.text);
        text.setText(R.string.recovery_mail);

        LinearLayout content = findViewById(R.id.Content);
        content.setVisibility(View.GONE);

        TextView recovery = findViewById(R.id.recovery);
        recovery.setVisibility(View.INVISIBLE);
        recovery.setEnabled(false);

       /* recovery.setText(R.string.next_2);
        recovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ReservRecovery.this, EnterCode.class));
            }
        });*/

        final TextView loginHint = findViewById(R.id.loginHint);
        loginHint.setText(R.string.email);

        final Button buttonNext = findViewById(R.id.buttonNext);
        final LinearLayout divider = findViewById(R.id.divider);

        passwordText = findViewById(R.id.enter);
        passwordText.setHint(R.string.email);
        passwordText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case reservMethod:
                    close.setEnabled(true);
                    close.setVisibility(View.VISIBLE);
                    close.setImageResource(R.drawable.ic_back);
                    break;
                case createUserAccount:
                    title.setText(R.string.create_user_account);
                    text.setText(R.string.recovery_mail);

                    close.setVisibility(View.VISIBLE);
                    close.setEnabled(true);
                    close.setImageResource(R.drawable.ic_back);
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                    break;
                case changeEmail:
                    title.setText(R.string.security_lk);
                    text.setText(R.string.recovery_mail);

                    close.setVisibility(View.VISIBLE);
                    close.setEnabled(true);
                    close.setImageResource(R.drawable.ic_back);
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });
                    break;
            }
        }

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (passwordText.length() > 0) {
                    buttonNext.setEnabled(true);
                    buttonNext.setTextColor(StaticData.colorWhite);
                    buttonNext.setBackgroundResource(R.drawable.round_message_red);
                    loginHint.setVisibility(View.VISIBLE);
                    divider.setBackgroundColor(StaticData.colorRed);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (2 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                } else {
                    buttonNext.setEnabled(false);
                    buttonNext.setTextColor(StaticData.colorGrayText);
                    buttonNext.setBackgroundResource(R.drawable.round_gray);
                    loginHint.setVisibility(View.INVISIBLE);
                    divider.setBackgroundColor(StaticData.colorGray);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (1 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        passwordText.addTextChangedListener(watcher);
    }

    public void onClickNext(View view) {
        Intent intent = new Intent(this, ConfirmationRecovery.class);
        intent.putExtra("send_code", passwordText.getText().toString());
        intent.putExtra("type", type);
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }
}
