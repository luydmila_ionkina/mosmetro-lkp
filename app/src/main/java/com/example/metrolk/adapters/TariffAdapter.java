package com.example.metrolk.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.communication.ConversationMM;
import com.example.metrolk.tariff.Tariff;

public class TariffAdapter extends RecyclerView.Adapter<TariffAdapter.ViewHolder>{

    private LayoutInflater inflater;
    private Context context;
    private String tabNameFirstPage[] = {"Билет «Единый»", "Проездной ТАТ"};
    private String tabNameSecondPage[] = {"Транспортная карта «Тройка»", "Социальная карта Москвича"};
    private int imageFirstPage[] = {R.drawable.tariff_1, R.drawable.tariff_2};
    private int imageSecondPage[] = {R.drawable.tariff_3, R.drawable.tariff_4};
    private int count = 2;
    private int page;

    public TariffAdapter(Context context, int p) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.page = p;
    }

    @Override
    public TariffAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.tariff, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TariffAdapter.ViewHolder holder, final int position) {
        if(page == 0) createFirstPage(holder, position);
        if(page == 1) createSecondPage(holder, position);

        holder.layoutTariff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Tariff.class);
                intent.putExtra("type", StaticData.ActivityType.infoTariff);
                if(page == 0) {
                    intent.putExtra("name", tabNameFirstPage[position]);
                }
                if(page == 1) {
                    intent.putExtra("name", tabNameSecondPage[position]);
                }
                context.startActivity(intent);
            }
        });
    }

    public void createFirstPage(TariffAdapter.ViewHolder holder, int position){
        holder.name.setText(tabNameFirstPage[position]);
        holder.cardView.setImageResource(imageFirstPage[position]);
    }

    public void createSecondPage(TariffAdapter.ViewHolder holder, int position){
        holder.name.setText(tabNameSecondPage[position]);
        holder.cardView.setImageResource(imageSecondPage[position]);
    }

    @Override
    public int getItemCount() {
        return count;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout layoutTariff;
        ImageView cardView;
        TextView name;
        TextView description;

        ViewHolder(View view) {
            super(view);

            cardView = view.findViewById(R.id.cardView);
            name = view.findViewById(R.id.name);
            description = view.findViewById(R.id.description);
            layoutTariff = view.findViewById(R.id.LayoutTariff);
        }
    }
}
