package com.example.metrolk.recovery;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.payment.AdvanceAddition;
import com.example.metrolk.payment.WrittenSum;
import com.example.metrolk.payment.WrittenSumMetro;
import com.example.metrolk.profile.LastStation;
import com.example.metrolk.registration.ConfirmationRecovery;
import com.example.metrolk.registration.CreateUserAccount;
import com.example.metrolk.registration.CreateUserCode;
import com.example.metrolk.registration.CreateUserPassword;
import com.example.metrolk.registration.UserQuestion;

public class MethodRecovery extends AppCompatActivity {

    LinearLayout layoutMethod1, layoutMethod2;
    TextView title, text, title_1, text_1, title_2, text_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_method_recovery);

        layoutMethod1 = findViewById(R.id.layoutMethod1);
        layoutMethod2 = findViewById(R.id.layoutMethod2);

        title = findViewById(R.id.title);
        text = findViewById(R.id.text);

        title_1 = findViewById(R.id.title_1);
        text_1 = findViewById(R.id.text_1);

        title_2 = findViewById(R.id.title_2);
        text_2 = findViewById(R.id.text_2);

        StaticData.ActivityType type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case security: security();
                    break;
                case recoveryLogin: recoveryLogin();
                    break;
                case recoveryPassword: recoveryPassword();
                    break;
                case addTransportCard: addTransportCard();
                    break;
                case writtenScore: writtenScore();
            }
        }
    }

    public void security() {
        title.setText(R.string.security_lk);
        text.setText(R.string.security_lk_3);

        title_1.setText(R.string.change_phone);
        text_1.setText(R.string.change_phone_2);

        title_2.setText(R.string.change_email);
        text_2.setText(R.string.change_email_2);

        LinearLayout layoutMethod3 = findViewById(R.id.layoutMethod3);
        layoutMethod3.setVisibility(View.VISIBLE);

        TextView title_3 = findViewById(R.id.title_3);
        title_3.setText(R.string.password_account);

        TextView text_3 = findViewById(R.id.text_3);
        text_3.setText(R.string.password_account_2);

        layoutMethod1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MethodRecovery.this, CreateUserPassword.class);
                intent.putExtra("type", StaticData.ActivityType.changePhone);
                startActivity(intent);

                /*Intent intent = new Intent(MethodRecovery.this, CreateUserPassword.class);
                intent.putExtra("type", StaticData.ActivityType.controlQuestion);
                startActivity(intent);*/
            }
        });

        layoutMethod2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MethodRecovery.this, CreateUserPassword.class);
                intent.putExtra("type", StaticData.ActivityType.changeEmail);
                startActivity(intent);

                /*Intent intent = new Intent(MethodRecovery.this, CreateUserPassword.class);
                intent.putExtra("type", StaticData.ActivityType.reservMethod);
                startActivity(intent);*/
            }
        });

        layoutMethod3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MethodRecovery.this, CreateUserPassword.class);
                intent.putExtra("type", StaticData.ActivityType.changePassword);
                startActivity(intent);
            }
        });
    }

    public void recoveryLogin() {
        text.setText(R.string.recovery_login_3);

        title_1.setText(R.string.control_question);
        text_1.setText(R.string.control_question_2);

        title_2.setText(R.string.reserve_method);
        text_2.setText(R.string.reserve_method_2);

        layoutMethod1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MethodRecovery.this, UserQuestion.class);
                intent.putExtra("type", StaticData.ActivityType.recoveryLogin);
                startActivity(intent);
            }
        });

        layoutMethod2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MethodRecovery.this, ConfirmationRecovery.class);
                intent.putExtra("type", StaticData.ActivityType.recovery);
                startActivity(intent);
            }
        });
    }

    public void recoveryPassword() {
        layoutMethod1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MethodRecovery.this, CreateUserCode.class);
                intent.putExtra("type", StaticData.ActivityType.recoveryPassword);
                startActivity(intent);
            }
        });

        layoutMethod2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MethodRecovery.this, MethodRecovery.class);
                intent.putExtra("type", StaticData.ActivityType.recoveryLogin);
                startActivity(intent);
            }
        });
    }

    public void addTransportCard() {
        title.setText(R.string.add_card);
        text.setText(R.string.confirm_own_card);

        title_1.setText(R.string.two_last_trip);
        text_1.setText(R.string.two_last_trip_2);

        title_2.setText(R.string.advance_addition);
        text_2.setText(R.string.advance_addition_2);

        LinearLayout layoutMethod3 = findViewById(R.id.layoutMethod3);
        layoutMethod3.setVisibility(View.VISIBLE);

        TextView title_3 = findViewById(R.id.title_3);
        title_3.setText(R.string.mobile_tel);

        TextView text_3 = findViewById(R.id.text_3);
        text_3.setText(R.string.enter_mobile_tel);

        layoutMethod1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MethodRecovery.this, LastStation.class));
            }
        });

        layoutMethod2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MethodRecovery.this, AdvanceAddition.class));
            }
        });

        layoutMethod3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MethodRecovery.this, CreateUserAccount.class);
                intent.putExtra("type", StaticData.ActivityType.addCardConnectedPhone);
                startActivity(intent);
            }
        });
    }

    public void writtenScore() {
        ImageView close = findViewById(R.id.close);
        close.setVisibility(View.INVISIBLE);
        close.setEnabled(false);

        title.setText(R.string.written_sum);
        text.setText(R.string.written_sum_2);

        title_1.setText(R.string.written_smartphone);
        text_1.setText(R.string.written_smartphone_2);

        title_2.setText(R.string.written_terminal);
        text_2.setText(R.string.written_terminal_2);

        layoutMethod1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MethodRecovery.this, WrittenSum.class));
            }
        });

        layoutMethod2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MethodRecovery.this, WrittenSumMetro.class);
                intent.putExtra("type", StaticData.ActivityType.writtenScore);
                startActivity(intent);
            }
        });
    }

    public void onClickBack(View view) {
        finish();
    }
}
