package com.example.metrolk;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.StyleableRes;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makeramen.roundedimageview.RoundedImageView;

public class RoundButton extends LinearLayout {

    @StyleableRes
    int index0 = 0;

    TextView textView;
    RoundedImageView round;

    CharSequence text;
    int textColor, textSize, buttonColor, radius;
    String textFont, textTypeface;

    public RoundButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.round_button, this);

        int[] sets = {R.attr.text, R.attr.buttonColor};
        TypedArray typedArray = context.obtainStyledAttributes(attrs, sets);
        text = typedArray.getText(index0);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.RoundButton, 0, 0);
        textColor = a.getColor(R.styleable.RoundButton_textColor, 0xff000000);
        textSize = a.getInt(R.styleable.RoundButton_textSize, 0);
        textFont = a.getString(R.styleable.RoundButton_textFont);
        textTypeface = a.getString(R.styleable.RoundButton_textTypeface);
        buttonColor = a.getColor(R.styleable.RoundButton_buttonColor, 0xff000000);
        radius = a.getInt(R.styleable.RoundButton_radius, 0);

        typedArray.recycle();
        initComponents();

        setButtonStyle();
    }

    private void initComponents() {
        textView = findViewById(R.id.text);
        round = findViewById(R.id.round);
    }

    public void setButtonStyle() {
        if (text != null) textView.setText(text);
        if (textSize != 0) textView.setTextSize(textSize);
        if (textFont != null) {
            if (android.os.Build.VERSION.SDK_INT == Build.VERSION_CODES.O){
                if (textFont.equals("roboto_medium_"))
                    textView.setTypeface(getResources().getFont(R.font.roboto_medium));
                if (textFont.equals("roboto_regular_"))
                    textView.setTypeface(getResources().getFont(R.font.roboto_regular));
                if (textFont.equals("roboto_bold_"))
                    textView.setTypeface(getResources().getFont(R.font.roboto_bold));
            }
            else if(android.os.Build.VERSION.SDK_INT <= Build.VERSION_CODES.O) {
                if (textFont.equals("roboto_medium_"))
                    textView.setTypeface(ResourcesCompat.getFont(getContext(), R.font.roboto_medium));
                if (textFont.equals("roboto_regular_"))
                    textView.setTypeface(ResourcesCompat.getFont(getContext(), R.font.roboto_regular));
                if (textFont.equals("roboto_bold_"))
                    textView.setTypeface(ResourcesCompat.getFont(getContext(), R.font.roboto_bold));
            }
        }
        if (textTypeface != null) {
            if (textTypeface.equals("normal"))
                textView.setTypeface(textView.getTypeface(), Typeface.NORMAL);
        }

        textView.setTextColor(textColor);
        round.setBackgroundColor(buttonColor);

        float density = getResources().getDisplayMetrics().density;
        if (radius != 0) {
            round.setCornerRadius((int) (radius * density + 0.5f));
            round.setOval(false);
        }
    }

    public void setButtonText(String text) {
        textView.setText(text);
    }

    public void setTextColor(int textColor) {
        textView.setTextColor(textColor);
    }

    public void setBackgroundColor(int buttonColor) {
        round.setBackgroundColor(buttonColor);
    }

}