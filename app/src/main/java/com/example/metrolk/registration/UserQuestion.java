package com.example.metrolk.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.MainActivity;
import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.profile.Profile;

public class UserQuestion extends AppCompatActivity {

    StaticData.ActivityType type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization);

        findViewById(R.id.enter).requestFocus();

        ImageView close = findViewById(R.id.close);
        close.setImageResource(R.drawable.ic_back);

        ImageView question = findViewById(R.id.question);
        question.setEnabled(true);
        question.setVisibility(View.INVISIBLE);

        TextView title = findViewById(R.id.title);
        title.setText(R.string.create_user_account);

        LinearLayout content = findViewById(R.id.Content);
        content.setVisibility(View.GONE);

        TextView text = findViewById(R.id.text);
        text.setText(R.string.answer_control_q);

        TextView recovery = findViewById(R.id.recovery);
        recovery.setVisibility(View.INVISIBLE);
        recovery.setEnabled(false);

        final TextView loginHint = findViewById(R.id.loginHint);
        loginHint.setText(R.string.answer);

        final Button buttonNext = findViewById(R.id.buttonNext);

        TextView text_2 = findViewById(R.id.text_2);
        text_2.setVisibility(View.VISIBLE);

        if (getIntent().getStringExtra("question") != null)
            text_2.setText(getIntent().getStringExtra("question"));
        else text_2.setText(R.string.question_1);

        final EditText passwordText = findViewById(R.id.enter);
        passwordText.setHint(R.string.answer);
        passwordText.setInputType(InputType.TYPE_CLASS_TEXT);

        final LinearLayout divider = findViewById(R.id.divider);

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (passwordText.length() > 0) {
                    buttonNext.setEnabled(true);
                    buttonNext.setTextColor(StaticData.colorWhite);
                    buttonNext.setBackgroundResource(R.drawable.round_message_red);
                    loginHint.setVisibility(View.VISIBLE);
                    divider.setBackgroundColor(StaticData.colorRed);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (2 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                } else {
                    buttonNext.setEnabled(false);
                    buttonNext.setTextColor(StaticData.colorGrayText);
                    buttonNext.setBackgroundResource(R.drawable.round_gray);
                    loginHint.setVisibility(View.INVISIBLE);
                    divider.setBackgroundColor(StaticData.colorGray);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (1 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        passwordText.addTextChangedListener(watcher);

        type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case recoveryLogin:
                    title.setText(R.string.recovery_state);
                    break;
                case controlQuestion:
                    title.setText(R.string.control_question);
                    buttonNext.setText(R.string.save);

                    buttonNext.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(UserQuestion.this, Profile.class));
                        }
                    });
                    break;
            }
        }
    }

    public void onClickNext(View view) {
        if(type != null) {
            switch(type) {
                case recoveryLogin:
                     /*StaticData.showDialog(getString(R.string.error), getString(R.string.error_invalid_answer),
                getString(R.string.close), null, this);*/

                    Intent intent = new Intent(this, CreateUserAccount.class);
                    intent.putExtra("type", StaticData.ActivityType.recoveryLogin);
                    startActivity(intent);
                    break;
                case controlQuestion:
                    break;
            }
        } else startActivity(new Intent(this, ReservRecovery.class));
    }

    public void onClickBack(View view) {
        finish();
    }
}
