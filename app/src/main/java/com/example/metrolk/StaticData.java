package com.example.metrolk;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.example.metrolk.profile.RemoveScore;

public class StaticData {

    public enum ActivityType { authorized, notAuthorized,
        newCode, changeCode, changePassword, newPassword, changePhone, changeEmail,
        recoveryLogin, controlQuestion, reservMethod, recoveryPassword, security, recoveryAccess, recovery, reservRecovery,
        messagePanel, createUserAccount, addCardConnectedPhone,
        infoTariff, ticketCard, autoPayment,
        accompanying, answered, conversation,
        renameCard, addBankCard, addTransportCard, additionScore, removeScore, rulesRemovingScore, writtenScore,
        mos_ru, socialNetwork }
    public enum Card { limited, social }
    public enum CardType { visa, masterCard, mir, unionPay, americanExpress, jcb }
    public enum Element { panelCards,  myCards, additionScore,
        checkBankCard, checkTransportCard, bankCard, transportCard,
        trip, period, operation }
    public enum News { all, notification, proposal, poll }
    public enum History { trip, operation }
    public enum Checklist { checklist, radioGroup }
    public enum Map { map, terminal }
    public enum Terminal { searchTerminal, imageTerminal }

    public static final int colorRed = Color.parseColor("#DA2032");
    public static final int colorGreen = Color.parseColor("#34A626");
    public static final int colorWhite = Color.parseColor("#FFFFFF");
    public static final int colorRose = Color.parseColor("#FAF0F1");
    public static final int colorGray = Color.parseColor("#F5F5F5");
    public static final int colorGrayText = Color.parseColor("#AFADAC");
    public static final int colorBlack = Color.parseColor("#000000");

    public static final int colorVisa = Color.parseColor("#1A1F71");
    public static final int colorMasterCard = Color.parseColor("#333333");
    public static final int colorMir = Color.parseColor("#05754D");
    public static final int colorUnionPay = Color.parseColor("#007B84");
    public static final int colorAmericanExpress = Color.parseColor("#006FCF");
    public static final int colorJCB = Color.parseColor("#0D275A");

    public static void activateRoundButton(RoundButton button) {
        button.setBackgroundColor(colorRed);
        button.setTextColor(colorWhite);
    }

    public static void disactivateRoundButton(RoundButton button) {
        button.setBackgroundColor(colorGray);
        button.setTextColor(colorGrayText);
    }

    public static void showDialog(String title, String text, String buttonText, View.OnClickListener onClickButton, Context context) {
        View dialogView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog, null);

        TextView titleView = dialogView.findViewById(R.id.title);
        titleView.setText(title);

        TextView textView = dialogView.findViewById(R.id.text);
        textView.setText(text);

        RoundButton button = dialogView.findViewById(R.id.button);
        button.setButtonText(buttonText);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialogView)
                .setCancelable(false);
        final AlertDialog alert = builder.create();
        alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alert.show();

        if (onClickButton != null) button.setOnClickListener(onClickButton);
        else button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.dismiss();
                }
            });
    }

    public static void showDialogTwoButton(String title, String text,
               String buttonText1, View.OnClickListener onClickButton2, String buttonText2, Context context) {

        View dialogView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_2, null);

        TextView titleView = dialogView.findViewById(R.id.title);
        titleView.setText(title);

        TextView textView = dialogView.findViewById(R.id.text);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            textView.setText(Html.fromHtml(text,  Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        else textView.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        RoundButton button1 = dialogView.findViewById(R.id.button_1);
        button1.setButtonText(buttonText1);

        RoundButton button2 = dialogView.findViewById(R.id.button_2);
        button2.setButtonText(buttonText2);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialogView)
                .setCancelable(false);
        final AlertDialog alert = builder.create();
        alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alert.show();

        if(onClickButton2 != null) button2.setOnClickListener(onClickButton2);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
    }

    public static void showDialogThreeButton(final Context context) {
        View dialogView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_3, null);
        TextView textView = dialogView.findViewById(R.id.text);

        String text = "Карта <b>1234 567 890</b> будет заблокирована. <br></br><br></br>" +
                "Вы можете перенести баланс и заблокировать карту или выполнить блокировку без переноса.";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            textView.setText(Html.fromHtml(text,  Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        else textView.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialogView)
                .setCancelable(false);
        final AlertDialog alert = builder.create();
        alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alert.show();

        RoundButton button1 = dialogView.findViewById(R.id.button_1);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        RoundButton button2 = dialogView.findViewById(R.id.button_2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RemoveScore.class);
                intent.putExtra("type", ActivityType.removeScore);
                context.startActivity(intent);
            }
        });

        RoundButton button3 = dialogView.findViewById(R.id.button_3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
    }
}
