package com.example.metrolk.adapters;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;

public class DateHistoryAdapter extends RecyclerView.Adapter<DateHistoryAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private Context context;
    private StaticData.History type;
    private int count;
    private String dateTrip[] = { "Сегодня" };
    private String dateOperation[] = { "Сегодня", "Вчера" };
    private int countOperation[] = { 2, 5 };

    public DateHistoryAdapter(Context context, StaticData.History type, int count) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.type = type;
        this.count = count;
    }

    @Override
    public DateHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_date, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final DateHistoryAdapter.ViewHolder holder, final int position) {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        holder.recyclerView.setLayoutManager(linearLayoutManager);

        if(type != null){
            switch(type) {
                case trip:
                    holder.date_str.setText(dateTrip[position]);
                    holder.recyclerView.setAdapter(new HistoryAdapter(context, StaticData.History.trip, 2));
                    break;
                case operation:
                    holder.date_str.setText(dateOperation[position]);
                    holder.recyclerView.setAdapter(new HistoryAdapter(context, StaticData.History.operation, countOperation[position]));
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return count;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerView recyclerView;
        TextView date_str;

        ViewHolder(View view) {
            super(view);

            recyclerView = view.findViewById(R.id.News);
            date_str = view.findViewById(R.id.date_str);
        }
    }
}