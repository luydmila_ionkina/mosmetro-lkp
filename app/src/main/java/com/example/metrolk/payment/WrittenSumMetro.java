package com.example.metrolk.payment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.metrolk.MainActivity;
import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;
import com.example.metrolk.adapters.MyCardAdapter;
import com.example.metrolk.adapters.TerminalAdapter;
import com.example.metrolk.map.MapPayments;
import com.example.metrolk.profile.CardConnected;

public class WrittenSumMetro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_written_sum);

        ImageView back = findViewById(R.id.back);

        TextView title = findViewById(R.id.title);
        TextView text = findViewById(R.id.text);

        ImageView image = findViewById(R.id.image);
        image.setVisibility(View.INVISIBLE);

        LinearLayout layoutImage = findViewById(R.id.layoutImage);

        RoundButton next = findViewById(R.id.next);
        next.setVisibility(View.VISIBLE);

        RelativeLayout backLKP = findViewById(R.id.backLKP);

        StaticData.ActivityType type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case recovery: recovery(back, title, text, next, backLKP);
                    break;
                case writtenScore: writtenScore(back, title, text, layoutImage, next, backLKP);
                    break;
            }
        }
    }

    public void recovery(ImageView back, TextView title, TextView text, RoundButton next, RelativeLayout backLKP) {
        back.setVisibility(View.INVISIBLE);

        title.setText(R.string.add_reserv_method);
        text.setText(R.string.add_reserv_method_2);

        RelativeLayout Content = findViewById(R.id.Content);
        Content.setVisibility(View.GONE);

        next.setButtonText(getString(R.string.back_to_lkp));
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WrittenSumMetro.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        backLKP.setVisibility(View.GONE);
    }

    public void writtenScore(ImageView back, TextView title, TextView text, LinearLayout layoutImage, RoundButton next, RelativeLayout backLKP) {
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        title.setText(R.string.write_sum_metro);
        text.setText(R.string.find_terminal);
        layoutImage.setVisibility(View.VISIBLE);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView terminalView = findViewById(R.id.terminalView);
        terminalView.setLayoutManager(linearLayoutManager);

        LinearSnapHelper snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                View centerView = findSnapView(layoutManager);
                if (centerView == null)
                    return RecyclerView.NO_POSITION;

                int position = layoutManager.getPosition(centerView);
                int targetPosition = -1;
                if (layoutManager.canScrollHorizontally()) {
                    if (velocityX < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }
                if (layoutManager.canScrollVertically()) {
                    if (velocityY < 0) {
                        targetPosition = position - 1;
                    } else {
                        targetPosition = position + 1;
                    }
                }
                final int firstItem = 0;
                final int lastItem = layoutManager.getItemCount() - 1;
                targetPosition = Math.min(lastItem, Math.max(targetPosition, firstItem));
                return targetPosition;
            }
        };
        snapHelper.attachToRecyclerView(terminalView);

        terminalView.setAdapter(new TerminalAdapter(this, StaticData.Terminal.imageTerminal));

        next.setButtonText(getString(R.string.map_payments));
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WrittenSumMetro.this, MapPayments.class));
            }
        });

        backLKP.setVisibility(View.VISIBLE);
        backLKP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WrittenSumMetro.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }
}
