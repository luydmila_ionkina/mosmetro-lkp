package com.example.metrolk.profile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.history.FilterOperation;
import com.example.metrolk.payment.AddCardScore;
import com.example.metrolk.payment.AdditionScore;
import com.example.metrolk.recovery.RecoveryAccess;
import com.example.metrolk.tariff.Tariff;
import com.example.metrolk.tariff.Tickets;
import com.makeramen.roundedimageview.RoundedImageView;

public class MMCardDescription extends AppCompatActivity {

    String nameCard;
    int backgroundCard;
    StaticData.Card card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mmcard_description);

        Toolbar toolbar = findViewById(R.id.toolbar);

        ImageView back = toolbar.findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        nameCard = getIntent().getStringExtra("name");
        backgroundCard = getIntent().getIntExtra("background", 0);

        if(nameCard != null) {
            TextView textBar = findViewById(R.id.textBar);
            textBar.setText(nameCard);
        }

        if(backgroundCard != 0) {
            /*RelativeLayout layoutCard = findViewById(R.id.LayoutCard);
            layoutCard.setBackgroundResource(backgroundCard);*/

            RoundedImageView backgroundView = findViewById(R.id.background);
            backgroundView.setBackgroundResource(backgroundCard);
        }

        TextView name = findViewById(R.id.name);
        name.setText(nameCard);

        card = (StaticData.Card) getIntent().getSerializableExtra("type");
        if(card != null) {
            switch(card) {
                case social:
                    TextView typeCard = findViewById(R.id.nameCard);
                    typeCard.setText(R.string.card_social);

                    LinearLayout layoutValidFor = findViewById(R.id.ValidFor);
                    layoutValidFor.setVisibility(View.VISIBLE);

                    LinearLayout layoutBenefit = findViewById(R.id.TypeBenefit);
                    layoutBenefit.setVisibility(View.VISIBLE);
                    break;
                case limited:
                    break;
            }
        }

        View.OnClickListener buttonClick1 = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MMCardDescription.this, AdditionScore.class));
            }
        };

        StaticData.showDialogTwoButton(getString(R.string.write_addition_score), getString(R.string.write_addition_score_2),
                getString(R.string.close), buttonClick1, getString(R.string.next_3), this);
    }

    public void onClickWaitting(View view) {
        startActivity(new Intent(this, AdditionScore.class));
    }

    public void onClickStatusCard(View view) {
        startActivity(new Intent(this, StatusCard.class));
    }

    public void onClickAddCardScore(View view) {
        startActivity(new Intent(this, AddCardScore.class));
    }

    public void onClickTickets(View view) {
        startActivity(new Intent(this, Tickets.class));
    }

    public void onClickAutoPayment(View view) {
        Intent intent = new Intent(this, Tariff.class);
        intent.putExtra("type", StaticData.ActivityType.autoPayment);
        startActivity(intent);
    }

    public void onClickRemoveScore(View view) {
        Intent intent = new Intent(this, RemoveScore.class);
        intent.putExtra("type", StaticData.ActivityType.removeScore);
        intent.putExtra("name", nameCard);
        intent.putExtra("background", backgroundCard);
        intent.putExtra("parentType", card);
        startActivity(intent);
    }

    public void onClickBonus(View view) {
        Intent intent = new Intent(this, FilterOperation.class);
        intent.putExtra("type", StaticData.History.operation); // bonus
        startActivity(intent);
    }

    public void onClickRename(View view) {
        Intent intent = new Intent(this, RecoveryAccess.class);
        intent.putExtra("type", StaticData.ActivityType.renameCard);
        startActivity(intent);
    }

    public void onClickDisconnectDevice(View view) {
        View.OnClickListener buttonClick1 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        };

        String text = "Карта <b>1234 567 890</b> будет отвязана от вашего аккаунта. <br></br><br></br>" +
                "После этого Вы сможете привязать её к этому или любому другому аккаунту.";

        StaticData.showDialogTwoButton(getString(R.string.disconnect_device), text,
                getString(R.string.cancel), buttonClick1, getString(R.string.disconnect), this);
    }

    public void onClickBlock(View view) {
        StaticData.showDialogThreeButton(this);
    }

    public void onClickHistoryTrip(View view) {
        Intent intent = new Intent(this, FilterOperation.class);
        intent.putExtra("type", StaticData.History.trip);
        startActivity(intent);
    }

    public void onClickHistoryOperation(View view) {
        Intent intent = new Intent(this, FilterOperation.class);
        intent.putExtra("type", StaticData.History.operation);
        startActivity(intent);
    }
}
