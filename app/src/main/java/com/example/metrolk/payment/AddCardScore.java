package com.example.metrolk.payment;

import android.content.Intent;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;
import com.example.metrolk.adapters.MyCardAdapter;
import com.example.metrolk.adapters.TripPeriodAdapter;
import com.makeramen.roundedimageview.RoundedImageView;

public class AddCardScore extends AppCompatActivity {

    int step1 = 0, step2 = 0, step3 = 0;
    BottomSheetDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card_score);

        Toolbar toolbar = findViewById(R.id.toolbar);

        ImageView back = toolbar.findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView textBar = findViewById(R.id.textBar);
        textBar.setText(R.string.add_score_2);
    }

    public void onClickNumberCard(View view) {
        View dialogView = View.inflate(this, R.layout.dialog_bottom_card, null);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        RecyclerView CardView = dialogView.findViewById(R.id.CardView);
        CardView.setLayoutManager(linearLayoutManager);
        CardView.setAdapter(new MyCardAdapter(this,
                getResources().getDisplayMetrics().density, StaticData.Element.transportCard, 3, null, null));

        LinearLayout enterNumberCard = dialogView.findViewById(R.id.EnterNumberCard);
        enterNumberCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddCardScore.this, AddCard.class);
                intent.putExtra("type", StaticData.ActivityType.additionScore);
                intent.putExtra("context", this.getClass());
                startActivity(intent);
            }
        });

        showDialog(dialogView);
    }

    public void onClickMethodPayment(View view) {
        View dialogView = View.inflate(this, R.layout.dialog_bottom_card, null);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        RecyclerView CardView = dialogView.findViewById(R.id.CardView);
        CardView.setLayoutManager(linearLayoutManager);
        CardView.setAdapter(new MyCardAdapter(this,
                getResources().getDisplayMetrics().density, StaticData.Element.bankCard, 3, null, null));

        TextView name = dialogView.findViewById(R.id.name);
        name.setText(R.string.bank_card);

        LinearLayout enterNumberCard = dialogView.findViewById(R.id.EnterNumberCard);
        enterNumberCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        showDialog(dialogView);
    }

    public void onClickTickets(View view) {
        View dialogView = View.inflate(this, R.layout.dialog_bottom_card, null);

        float density = getResources().getDisplayMetrics().density;

        LinearLayout layoutTariff = dialogView.findViewById(R.id.layoutTariff);
        layoutTariff.setPadding((int) (0 * density + 0.5f), (int) (0 * density + 0.5f), (int) (0 * density + 0.5f), (int) (132 * density + 0.5f));
        layoutTariff.setVisibility(View.VISIBLE);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        RecyclerView Trip = dialogView.findViewById(R.id.Trip);
        Trip.setLayoutManager(linearLayoutManager);
        Trip.setAdapter(new TripPeriodAdapter(this, 2, StaticData.Element.trip));

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        RecyclerView TariffPeriod = dialogView.findViewById(R.id.TariffPeriod);
        TariffPeriod.setLayoutManager(linearLayoutManager);
        TariffPeriod.setAdapter(new TripPeriodAdapter(this, 5, StaticData.Element.trip));

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        RecyclerView SocialCard = dialogView.findViewById(R.id.SocialCard);
        SocialCard.setLayoutManager(linearLayoutManager);
        SocialCard.setAdapter(new TripPeriodAdapter(this, 3, StaticData.Element.trip));

        RecyclerView CardView = dialogView.findViewById(R.id.CardView);
        CardView.setVisibility(View.GONE);

        LinearLayout enterNumberCard = dialogView.findViewById(R.id.EnterNumberCard);
        enterNumberCard.setVisibility(View.GONE);

        android.support.v7.widget.CardView enterSum = dialogView.findViewById(R.id.EnterSum);
        enterSum.setVisibility(View.VISIBLE);

        showDialog(dialogView);
    }

    public void showDialog(View dialogView) {
        dialog = new BottomSheetDialog(this, R.style.CustomBottomSheetDialogTheme);
        dialog.setCancelable(true);
        dialog.setContentView(dialogView);
        dialog.show();
    }

    public void dismissDialog() {
        if(dialog != null) dialog.dismiss();
    }

    public void selectCard1(String name, int background) {
        TextView text = findViewById(R.id.text1);
        text.setVisibility(View.INVISIBLE);

        LinearLayout layoutCard = findViewById(R.id.layoutCard1);
        layoutCard.setVisibility(View.VISIBLE);

        RoundedImageView imageCard = findViewById(R.id.imageCard1);
        imageCard.setImageResource(background);

        TextView nameCard = findViewById(R.id.nameCard1);
        nameCard.setText(name);

        step1++;
        if((step1 > 0) & (step2 > 0) & (step3 > 0)) buttonNextActivate();
        dismissDialog();
    }

    public void selectCard2(String name, int logo, int color) {
        float density = getResources().getDisplayMetrics().density;

        TextView text = findViewById(R.id.text2);
        text.setVisibility(View.INVISIBLE);

        LinearLayout layoutCard = findViewById(R.id.layoutCard2);
        layoutCard.setVisibility(View.VISIBLE);

        RoundedImageView imageCard = findViewById(R.id.imageCard2);
        imageCard.setImageResource(logo);
        imageCard.setBackgroundColor(color);

        imageCard.setPadding((int) (4 * density + 0.5f), (int) (4 * density + 0.5f), (int) (4 * density + 0.5f), (int) (4 * density + 0.5f));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins((int) (0 * density + 0.5f), (int) (0 * density + 0.5f), (int) (16 * density + 0.5f), (int) (0 * density + 0.5f));
        layoutParams.height = (int) (32 * density + 0.5f);
        layoutParams.width = (int) (32 * density + 0.5f);
        imageCard.setLayoutParams(layoutParams);
        imageCard.setCornerRadius((int) (6.4 * density + 0.5f));

        TextView nameCard = findViewById(R.id.nameCard2);
        nameCard.setText(name);

        step2++;
        if((step1 > 0) & (step2 > 0) & (step3 > 0)) buttonNextActivate();
        dismissDialog();
    }

    public void selectCard3(/*String name, int background*/) {
        TextView text = findViewById(R.id.text3);
        text.setVisibility(View.INVISIBLE);

        LinearLayout layoutCard = findViewById(R.id.layoutCard3);
        layoutCard.setVisibility(View.VISIBLE);

        RoundButton imageCard = findViewById(R.id.imageCard3);
        //imageCard.setImageResource(background);

        TextView nameCard = findViewById(R.id.nameCard3);
        nameCard.setText("Единый - 60 поездок");

        step3++;
        if((step1 > 0) & (step2 > 0) & (step3 > 0)) buttonNextActivate();
        dismissDialog();
    }

    public void buttonNextActivate() {
        RoundButton next = findViewById(R.id.next);
        next.setBackgroundColor(StaticData.colorRed);
        next.setTextColor(StaticData.colorWhite);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddCardScore.this, PaymentFinish.class);
                startActivity(intent);
            }
        });
    }
}
