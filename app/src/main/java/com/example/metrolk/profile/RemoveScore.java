package com.example.metrolk.profile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;

public class RemoveScore extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remove_score);

        StaticData.ActivityType type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case removeScore: removeScore();
                    break;
                case rulesRemovingScore: rulesRemovingScore();
                    break;
            }
        }
    }

    public void removeScore() {
        RoundButton next = findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RemoveScore.this, RemoveScore.class);
                intent.putExtra("type", StaticData.ActivityType.rulesRemovingScore);
                startActivity(intent);
            }
        });
    }

    public void rulesRemovingScore() {
        TextView title = findViewById(R.id.title);
        title.setText(R.string.rules_removing);

        ImageView info = findViewById(R.id.info);
        info.setVisibility(View.GONE);

        TextView text = findViewById(R.id.text);
        text.setText(R.string.empty_text);

        LinearLayout layoutDevice = findViewById(R.id.Device);
        layoutDevice.setVisibility(View.GONE);

        LinearLayout layoutConfirm = findViewById(R.id.Confirm);
        layoutConfirm.setVisibility(View.VISIBLE);

        final RoundButton buttonNext = findViewById(R.id.next);
        buttonNext.setButtonText("Перенести");
        buttonNext.setEnabled(false);
        buttonNext.setTextColor(StaticData.colorGrayText);
        buttonNext.setBackgroundColor(StaticData.colorGray);

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RemoveScore.this, MyCard.class);
                startActivity(intent);
            }
        });

        CheckBox checkBox = findViewById(R.id.checkBox);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    buttonNext.setEnabled(true);
                    buttonNext.setTextColor(StaticData.colorWhite);
                    buttonNext.setBackgroundColor(StaticData.colorRed);
                }
                else {
                    buttonNext.setEnabled(false);
                    buttonNext.setTextColor(StaticData.colorGrayText);
                    buttonNext.setBackgroundColor(StaticData.colorGray);
                }
            }
        });
    }

    public void onClickCardDescription(View view) {
        Intent intent = new Intent(this, MMCardDescription.class);
        intent.putExtra("name", getIntent().getStringExtra("name"));
        intent.putExtra("background", getIntent().getIntExtra("background", 0));
        intent.putExtra("type", getIntent().getSerializableExtra("parentType"));
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }
}
