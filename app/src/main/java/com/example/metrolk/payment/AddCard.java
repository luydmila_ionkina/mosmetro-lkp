package com.example.metrolk.payment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.metrolk.PhoneEditText;
import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.autorization.GosServices;
import com.example.metrolk.profile.EnterNameCard;

public class AddCard extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization);

        findViewById(R.id.enter).requestFocus();

        ImageView question = findViewById(R.id.question);
        TextView title = findViewById(R.id.title);
        TextView text = findViewById(R.id.text);
        TextView loginHint = findViewById(R.id.loginHint);
        EditText passwordText = findViewById(R.id.enter);
        Button buttonNext = findViewById(R.id.buttonNext);

        LinearLayout content = findViewById(R.id.Content);
        content.setVisibility(View.GONE);

        TextView recovery = findViewById(R.id.recovery);
        recovery.setVisibility(View.INVISIBLE);
        recovery.setEnabled(false);

        final LinearLayout divider = findViewById(R.id.divider);

        StaticData.ActivityType type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case addBankCard: addionBankCard(question, title, text, loginHint, passwordText, buttonNext, divider);
                    break;
                case addTransportCard: additionTransportCard(question, title, text, loginHint, passwordText, buttonNext, divider);
                    break;
                case additionScore: additionScore(question, title, text, loginHint, passwordText, buttonNext, divider);
                    break;
            }
        }
    }

    public void addionBankCard(ImageView question, TextView title, TextView text,
                final TextView loginHint, final EditText passwordText, final Button buttonNext, final LinearLayout divider) {

        question.setEnabled(true);
        question.setVisibility(View.INVISIBLE);

        title.setText(R.string.card_name);
        text.setText(R.string.card_name_2);
        loginHint.setText(R.string.card_name_3);

        passwordText.setInputType(InputType.TYPE_CLASS_TEXT);
        passwordText.setHint(R.string.card_name);

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (passwordText.length() > 0) {
                    buttonNext.setEnabled(true);
                    buttonNext.setTextColor(StaticData.colorWhite);
                    buttonNext.setBackgroundResource(R.drawable.round_message_red);
                    loginHint.setVisibility(View.VISIBLE);
                    divider.setBackgroundColor(StaticData.colorRed);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (2 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                } else {
                    buttonNext.setEnabled(false);
                    buttonNext.setTextColor(StaticData.colorGrayText);
                    buttonNext.setBackgroundResource(R.drawable.round_gray);
                    loginHint.setVisibility(View.INVISIBLE);
                    divider.setBackgroundColor(StaticData.colorGray);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (1 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        passwordText.addTextChangedListener(watcher);
    }

    public void additionTransportCard(ImageView question, TextView title, TextView text,
               final TextView loginHint, final EditText passwordText, final Button buttonNext, LinearLayout divider) {

        question.setImageResource(R.drawable.ic_nfc);
        question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticData.showDialog(getString(R.string.nfc_on), getString(R.string.nfc_on_2),
                        getString(R.string.close), null, AddCard.this);
            }
        });

        title.setText(R.string.add_card);
        setTextWatcher(question, text, loginHint, passwordText, buttonNext, divider);
    }

    public void additionScore(ImageView question, TextView title, TextView text,
                              final TextView loginHint, final EditText passwordText, final Button buttonNext, LinearLayout divider) {

        title.setText(R.string.number_device);
        setTextWatcher(question, text, loginHint, passwordText, buttonNext, divider);
    }

    public void setTextWatcher(ImageView question, TextView text, final TextView loginHint,
                               final EditText passwordText, final Button buttonNext, final LinearLayout divider) {
        question.setImageResource(R.drawable.ic_nfc);
        question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticData.showDialog(getString(R.string.nfc_on), getString(R.string.nfc_on_2),
                        getString(R.string.close), null, AddCard.this);
            }
        });

        text.setText(R.string.number_transport_card_2);
        loginHint.setText(R.string.number_transport_card);
        passwordText.setInputType(InputType.TYPE_CLASS_NUMBER);
        passwordText.setHint(R.string.number_transport_card);

        final RelativeLayout layoutCard = findViewById(R.id.LayoutCard);

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (passwordText.length() > 0) {
                        loginHint.setVisibility(View.VISIBLE);
                        divider.setBackgroundColor(StaticData.colorRed);

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (2 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                }
                if(passwordText.length() >= 10) {
                    buttonNext.setEnabled(true);
                    buttonNext.setTextColor(StaticData.colorWhite);
                    buttonNext.setBackgroundResource(R.drawable.round_message_red);

                    layoutCard.setVisibility(View.VISIBLE);
                }
                if(passwordText.length() <= 0) {
                    loginHint.setVisibility(View.INVISIBLE);
                    divider.setBackgroundColor(StaticData.colorGray);

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (1 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                }
                if(passwordText.length() < 10) {
                    buttonNext.setEnabled(false);
                    buttonNext.setTextColor(StaticData.colorGrayText);
                    buttonNext.setBackgroundResource(R.drawable.round_gray);

                    layoutCard.setVisibility(View.GONE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        passwordText.addTextChangedListener(watcher);
    }

    public void onClickNext(View view) {

        StaticData.ActivityType type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        switch(type) {
            case addBankCard: startActivity(new Intent(this, GosServices.class));
                break;
            case addTransportCard: startActivity(new Intent(this, EnterNameCard.class));
                break;
            case additionScore:
                /*Context context = (Context) getIntent().getSerializableExtra("context");
                if((context != null) && (context.getClass() == AddCardScore.class)) {
                    AddCardScore addCardScore = (AddCardScore) context;
                    addCardScore.selectCard1("Моя тройка", R.drawable.background_card_1);
                }*/
                break;
        }
    }

    public void onClickBack(View view) {
        finish();
    }
}
