package com.example.metrolk.payment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.history.FilterOperation;

public class CardDescription extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_description);

        Toolbar toolbar = findViewById(R.id.toolbar);

        ImageView back = toolbar.findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ImageView edit = toolbar.findViewById(R.id.edit);
        edit.setVisibility(View.VISIBLE);

        TextView textBar = findViewById(R.id.textBar);

        String nameCard = getIntent().getStringExtra("name");
        if(nameCard != null) textBar.setText(nameCard);
        else textBar.setText(R.string.my_card);
    }

    public void onClickOperation(View view) {
        Intent intent = new Intent(this, FilterOperation.class);
        intent.putExtra("type", StaticData.History.operation);
        startActivity(intent);
    }
}
