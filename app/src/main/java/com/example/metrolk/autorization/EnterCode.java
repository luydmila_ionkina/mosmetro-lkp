package com.example.metrolk.autorization;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.metrolk.MainActivity;
import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.profile.Profile;

public class EnterCode extends AppCompatActivity {

    private int position = 0, enter = 1, codeFirst = 0, codeSecond = 0;
    private String code = "";
    private ImageView imageCode1, imageCode2, imageCode3, imageCode4, imageCode5, imageDel, imageBack;
    private ImageView codeView[];
    private TextView title, text, next;
    private boolean codeOff = false;
    StaticData.ActivityType type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_code);

        imageCode1 = findViewById(R.id.imageCode1);
        imageCode2 = findViewById(R.id.imageCode2);
        imageCode3 = findViewById(R.id.imageCode3);
        imageCode4 = findViewById(R.id.imageCode4);
        imageCode5 = findViewById(R.id.imageCode5);
        codeView = new ImageView[]{imageCode1, imageCode2, imageCode3, imageCode4, imageCode5};

        imageDel = findViewById(R.id.delete);
        imageDel.setEnabled(true);

        imageBack = findViewById(R.id.back);
        imageBack.setEnabled(false);
        imageBack.setVisibility(View.INVISIBLE);

        title = findViewById(R.id.title);
        text = findViewById(R.id.text);
        next = findViewById(R.id.next);

        type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if (type != null) {
            switch(type) {
                case newCode:
                    break;
                case changeCode: changeCode();
                    break;
            }
        }
    }

    public void changeCode() {
        next.setText(R.string.code_off);
        imageBack.setVisibility(View.VISIBLE);
        imageBack.setEnabled(true);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                title.setText(R.string.code_off_2);
                text.setText(R.string.current_code);
                codeOff = true;
            }
        });
    }

    public void onPress1(View view) { setCode("1"); }
    public void onPress2(View view) { setCode("2"); }
    public void onPress3(View view) {
        setCode("3");
    }
    public void onPress4(View view) {
        setCode("4");
    }
    public void onPress5(View view) {
        setCode("5");
    }
    public void onPress6(View view) {
        setCode("6");
    }
    public void onPress7(View view) {
        setCode("7");
    }
    public void onPress8(View view) {
        setCode("8");
    }
    public void onPress9(View view) {
        setCode("9");
    }
    public void onPress0(View view) {
        setCode("0");
    }

    private void setCode(String c) {
        if (position < 5) {
            if (position < 0) {
                imageDel.setColorFilter(StaticData.colorGray);
                imageDel.setEnabled(false);
            } else {
                imageDel.setColorFilter(StaticData.colorRed);
                imageDel.setEnabled(true);
            }

            code = code + c;
            codeView[position].setBackgroundColor(StaticData.colorRed);
            position++;
        }
        if (position == 5) {
            //text.setText(R.string.enter_code_5);

            if (enter == 1) {
                codeFirst = Integer.parseInt(code);
                Log.d(" - - - - - code", codeFirst + "");

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try { Thread.sleep(500); }
                        catch (InterruptedException e) { e.printStackTrace(); }

                        EnterCode.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (codeOff) {
                                        startActivity(new Intent(EnterCode.this, Profile.class));
                                    }
                                    else {
                                        for (int i = 0; i < codeView.length; i++)
                                            codeView[i].setBackgroundColor(StaticData.colorGray);

                                        code = "";
                                        position = 0;
                                        enter = 2;
                                        title.setText(R.string.enter_code_3);
                                        text.setText(R.string.enter_code_4);
                                        next.setVisibility(View.GONE);

                                        imageBack.setEnabled(true);
                                        imageBack.setVisibility(View.VISIBLE);
                                    }
                                } catch (Exception e) { }
                            }
                        });

                    }
                }).start();

            } else {
                codeSecond = Integer.parseInt(code);
                Log.d(" - - - - - code", codeSecond + "");

                if (codeFirst == codeSecond) {
                    if (type != null) {
                        switch(type) {
                            case changeCode:
                                Intent intent = new Intent(EnterCode.this, FingerPrintEnter.class);
                                intent.putExtra("type", type);
                                startActivity(intent);
                                break;
                            case newCode:
                                Intent intent2 = new Intent(EnterCode.this, FingerPrintEnter.class);
                                intent2.putExtra("type", type);
                                startActivity(intent2);
                                break;
                        }
                    }
                }
                else {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try { Thread.sleep(500); }
                            catch (InterruptedException e) { e.printStackTrace(); }

                            EnterCode.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        StaticData.showDialog("Первый и второй код не совпадают!",
                                                "Попробуйте ввести код еще раз", "Ok", null, EnterCode.this);

                                        for (int i = 0; i < codeView.length; i++)
                                            codeView[i].setBackgroundColor(StaticData.colorGray);

                                        code = "";
                                        position = 0;
                                        enter = 1;
                                        title.setText(R.string.enter_code);
                                        text.setText(R.string.enter_code_2);
                                        next.setVisibility(View.VISIBLE);

                                        imageBack.setEnabled(false);
                                        imageBack.setVisibility(View.INVISIBLE);
                                    } catch (Exception e) { }
                                }
                            });

                        }
                    }).start();
                }
            }
        }
    }

    public void delCode(View view) {
        if ((position > 0) & (position < 5)) {
            imageDel.setColorFilter(StaticData.colorRed);
            imageDel.setEnabled(true);

            code = code.substring(0, code.length() - 1);
            codeView[position - 1].setBackgroundColor(StaticData.colorGray);
            position--;
        }
        if (position <= 0) {
            imageDel.setColorFilter(StaticData.colorGray);
            imageDel.setEnabled(false);
        }
    }

    public void onClickNext(View view) {
        finish();
        Intent intent = new Intent(this, FingerPrintEnter.class);
        intent.putExtra("type", StaticData.ActivityType.newCode);
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }
}
