package com.example.metrolk.registration;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.MainActivity;
import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.autorization.AutorizationActivity;

public class CreateUserCode extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization);

        findViewById(R.id.enter).requestFocus();

        ImageView close = findViewById(R.id.close);
        close.setImageResource(R.drawable.ic_back);

        ImageView question = findViewById(R.id.question);
        question.setEnabled(true);
        question.setVisibility(View.INVISIBLE);

        TextView title = findViewById(R.id.title);
        title.setText(R.string.create_user_account);

        LinearLayout content = findViewById(R.id.Content);
        content.setVisibility(View.GONE);

        TextView text = findViewById(R.id.text);
        String number = getString(R.string.enter_code_8);
        if (getIntent().getStringExtra("number") != null)
            number += " " + getIntent().getStringExtra("number");
        else number += " " + "+7 123 456 7890";
        text.setText(number);

        TextView recovery = findViewById(R.id.recovery);
        recovery.setText(R.string.code_send_with_error);
        recovery.setEnabled(false);

        final TextView loginHint = findViewById(R.id.loginHint);
        loginHint.setText(R.string.confirmation_code);

        final Button buttonNext = findViewById(R.id.buttonNext);
        buttonNext.setEnabled(false);
        buttonNext.setVisibility(View.INVISIBLE);

        final EditText passwordText = findViewById(R.id.enter);
        passwordText.setHint(R.string.confirmation_code);
        passwordText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);

        final LinearLayout divider = findViewById(R.id.divider);

        final StaticData.ActivityType type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case socialNetwork:
                case createUserAccount:
                    break;
                case recoveryLogin:
                case recoveryPassword:
                    title.setText(R.string.recovery_state);
                    buttonNext.setEnabled(false);
                    buttonNext.setVisibility(View.INVISIBLE);
                    break;
                case changePhone:
                    title.setText(R.string.security_lk);
                    break;
            }
        }

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(type != null) {
                    switch(type) {
                        case socialNetwork:
                            if (passwordText.length() == 4)
                                startActivity(new Intent(CreateUserCode.this, CreateUserPassword.class));
                            break;
                        case createUserAccount:
                            if (passwordText.length() > 0) {
                                loginHint.setVisibility(View.VISIBLE);
                                divider.setBackgroundColor(StaticData.colorRed);
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT);
                                layoutParams.height = (int) (2 * getResources().getDisplayMetrics().density + 0.5f);
                                divider.setLayoutParams(layoutParams);

                                if (passwordText.length() == 4) {
                                    Intent intent = new Intent(CreateUserCode.this, ReservRecovery.class);
                                    intent.putExtra("type", type);
                                    startActivity(intent);
                                }
                            } else {
                                loginHint.setVisibility(View.INVISIBLE);
                                divider.setBackgroundColor(StaticData.colorGray);
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT);
                                layoutParams.height = (int) (1 * getResources().getDisplayMetrics().density + 0.5f);
                                divider.setLayoutParams(layoutParams);
                            }
                            break;
                        case recoveryLogin:
                        case recoveryPassword:
                            if (passwordText.length() > 0) {
                                buttonNext.setEnabled(true);
                                buttonNext.setTextColor(StaticData.colorWhite);
                                buttonNext.setBackgroundResource(R.drawable.round_message_red);
                                loginHint.setVisibility(View.VISIBLE);
                                divider.setBackgroundColor(StaticData.colorRed);
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT);
                                layoutParams.height = (int) (2 * getResources().getDisplayMetrics().density + 0.5f);
                                divider.setLayoutParams(layoutParams);

                                if (passwordText.length() == 4) {

                                    switch(type) {
                                        case recoveryLogin:
                                             /*StaticData.showDialog(getString(R.string.error), getString(R.string.error_invalid_code),
                                      getString(R.string.close), null, CreateUserCode.this);*/

                                            View.OnClickListener buttonClick = new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Intent intent = new Intent(CreateUserCode.this, AutorizationActivity.class);
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(intent);
                                                }
                                            };

                                            StaticData.showDialog(getString(R.string.login_changed),
                                                    getString(R.string.login_changed_2),
                                                    getString(R.string.show_autorization), buttonClick, CreateUserCode.this);
                                            break;
                                        case recoveryPassword:
                                             /*StaticData.showDialog(getString(R.string.error), getString(R.string.error_invalid_code),
                                        getString(R.string.close), null, CreateUserCode.this);*/

                                            Intent intent = new Intent(CreateUserCode.this, CreateUserPassword.class);
                                            intent.putExtra("type", StaticData.ActivityType.recoveryPassword);
                                            startActivity(intent);
                                            break;
                                    }
                                }

                            } else {
                                switch(type){
                                    case recoveryLogin:
                                    case recoveryPassword:
                                        buttonNext.setEnabled(false);
                                        buttonNext.setTextColor(StaticData.colorGrayText);
                                        buttonNext.setBackgroundResource(R.drawable.round_gray);
                                        break;
                                    default: {
                                        loginHint.setVisibility(View.INVISIBLE);
                                        divider.setBackgroundColor(StaticData.colorGray);
                                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                                LinearLayout.LayoutParams.WRAP_CONTENT);
                                        layoutParams.height = (int) (1 * getResources().getDisplayMetrics().density + 0.5f);
                                        divider.setLayoutParams(layoutParams);
                                    }
                                }
                            }
                            break;
                        case changePhone:
                            if (passwordText.length() > 0) {
                                loginHint.setVisibility(View.VISIBLE);
                                divider.setBackgroundColor(StaticData.colorRed);
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT);
                                layoutParams.height = (int) (2 * getResources().getDisplayMetrics().density + 0.5f);
                                divider.setLayoutParams(layoutParams);

                                if (passwordText.length() == 4) {
                                    View.OnClickListener buttonClick = new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            SharedPreferences.Editor editor = MainActivity.mSettings.edit();
                                            editor.putString(MainActivity.AUTORIZATION, "authorized");
                                            editor.apply();

                                            Intent intent = new Intent(CreateUserCode.this, MainActivity.class);
                                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            intent.putExtra("type", StaticData.ActivityType.authorized);
                                            startActivity(intent);
                                        }
                                    };

                                    StaticData.showDialog(getString(R.string.phone_change),
                                            getString(R.string.phone_change_2),
                                            getString(R.string.show_personal_account), buttonClick, CreateUserCode.this);
                                }
                            } else {
                                loginHint.setVisibility(View.INVISIBLE);
                                divider.setBackgroundColor(StaticData.colorGray);
                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT);
                                layoutParams.height = (int) (1 * getResources().getDisplayMetrics().density + 0.5f);
                                divider.setLayoutParams(layoutParams);
                            }
                            break;
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
        passwordText.addTextChangedListener(watcher);
    }

    public void onClickNext(View view) {
        startActivity(new Intent(this, CreateUserPassword.class));
    }

    public void onClickBack(View view) {
        finish();
    }
}
