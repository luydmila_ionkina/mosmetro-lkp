package com.example.metrolk.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.metrolk.R;

public class TicketsAdapter extends RecyclerView.Adapter<TicketsAdapter.ViewHolder> {

    private final Context mContext;
    private LayoutInflater inflater;

    private String nameTicket[] = {"Единый - 60 поездок", "ТАТ - 60 поездок", "Единый - 365 дней"};
    private int colorTicket[] = {};

    public TicketsAdapter(Context c) {
        this.mContext = c;
        this.inflater = LayoutInflater.from(mContext);
    }

    @Override
    public TicketsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.ticket, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TicketsAdapter.ViewHolder holder, final int position) {
        holder.name.setText(nameTicket[position]);
        if(position != 0) holder.use.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return 3;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        TextView use;

        ViewHolder(View view) {
            super(view);

            name = view.findViewById(R.id.name);
            use = view.findViewById(R.id.use);
        }
    }
}