package com.example.metrolk.communication;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.adapters.PagerAdapter;

public class ConversationMM extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation_mm);

        Toolbar toolbar = findViewById(R.id.toolbar);

        ImageView back = toolbar.findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView textBar = findViewById(R.id.textBar);
        textBar.setText(R.string.conversation_mm);

        float density = getResources().getDisplayMetrics().density;
        final ViewPager viewPager = findViewById(R.id.viewpager);

        String type = getIntent().getStringExtra("type");
        if((type != null) && (type.equals("accompanying"))){
            textBar.setText(R.string.call_accompanying);

            String title[] = {"Новое обращение", "Мои обращения"};

            viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(),
                    ConversationMM.this, title, density, StaticData.ActivityType.accompanying));
            createTabs(viewPager);
        }
        else if((type != null) && (type.equals("sent"))) {
            LinearLayout layoutTabs = findViewById(R.id.LayoutTabs);
            layoutTabs.removeAllViews();

            View view = View.inflate(this, R.layout.answered, null);
            TextView state = view.findViewById(R.id.state);
            state.setVisibility(View.VISIBLE);
            layoutTabs.addView(view);
        }
        else if((type != null) && (type.equals("answered"))) {
            String title[] = {"Текст обращения", "Ответ"};

            viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(),
                    ConversationMM.this, title, density, StaticData.ActivityType.answered));
            createTabs(viewPager);
        }
        else {
            String title[] = {"Новое обращение", "Мои обращения"};

            viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager(),
                    ConversationMM.this, title, density, StaticData.ActivityType.conversation));
            createTabs(viewPager);
        }
    }

    public void createTabs(final ViewPager viewPager) {
        TabLayout tabLayout = findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        ViewTreeObserver viewTreeObserver = viewPager.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                int viewPagerWidth = viewPager.getWidth();
                float viewPagerHeight = (float) (viewPagerWidth * viewPager.getChildCount());

                /*float density = getResources().getDisplayMetrics().density;

                DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                float viewPagerHeight = (displayMetrics.heightPixels / displayMetrics.density) - (int) (96 * density + 0.5f);*/

                layoutParams.width = viewPagerWidth;
                layoutParams.height = (int) viewPagerHeight;

                viewPager.setLayoutParams(layoutParams);
                viewPager.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });
    }
}
