package com.example.metrolk.autorization;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.recovery.RecoveryAccess;

public class EnterPassword extends AppCompatActivity {

    EditText passwordText;
    String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization);

        findViewById(R.id.enter).requestFocus();

        ImageView close = findViewById(R.id.close);
        close.setImageResource(R.drawable.ic_back);

        ImageView question = findViewById(R.id.question);
        question.setEnabled(true);
        question.setVisibility(View.INVISIBLE);

        TextView title = findViewById(R.id.title);
        title.setText(R.string.enter_password);

        LinearLayout content = findViewById(R.id.Content);
        content.setVisibility(View.GONE);

        passwordText = findViewById(R.id.enter);
        passwordText.setHint(R.string.password);
        passwordText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        final Button buttonNext = findViewById(R.id.buttonNext);
        final TextView hint = findViewById(R.id.loginHint);
        hint.setText(R.string.password);

        final LinearLayout divider = findViewById(R.id.divider);

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (passwordText.length() > 0) {
                    hint.setVisibility(View.VISIBLE);
                    divider.setBackgroundColor(StaticData.colorRed);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (2 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                }

                if (passwordText.length() > 6) {
                    buttonNext.setEnabled(true);
                    buttonNext.setTextColor(StaticData.colorWhite);
                    buttonNext.setBackgroundResource(R.drawable.round_message_red);
                }
                if (passwordText.length() <= 6) {
                    buttonNext.setEnabled(false);
                    buttonNext.setTextColor(StaticData.colorGrayText);
                    buttonNext.setBackgroundResource(R.drawable.round_gray);
                }
                if (passwordText.length() <= 0) {
                    hint.setVisibility(View.INVISIBLE);
                    divider.setBackgroundColor(StaticData.colorGray);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (1 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        passwordText.addTextChangedListener(watcher);
    }

    public void onClickNext(View view) {
        password = passwordText.getText().toString();

        /*StaticData.showDialog(getString(R.string.user_doesnt_exist),
                getString(R.string.user_doesnt_exist_2), "Закрыть", null, this);*/

        Intent intent = new Intent(this, EnterCode.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("type", StaticData.ActivityType.newCode);
        startActivity(intent);
    }

    public void onClickRecovery(View view) {
        Intent intent = new Intent(this, RecoveryAccess.class);
        intent.putExtra("type", StaticData.ActivityType.recoveryAccess);
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }
}
