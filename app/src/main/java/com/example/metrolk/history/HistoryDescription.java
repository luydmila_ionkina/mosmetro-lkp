package com.example.metrolk.history;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;

public class HistoryDescription extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_description);

        Toolbar toolbar = findViewById(R.id.toolbar);

        ImageView back = toolbar.findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        TextView textBar = findViewById(R.id.textBar);

        StaticData.History type = (StaticData.History) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case trip: textBar.setText(R.string.travel);
                    break;
                case operation: textBar.setText(R.string.history_operations);
                operation();
                    break;
            }
        }
    }

    public void operation() {
        String title = getIntent().getStringExtra("title");

        TextView title_1 = findViewById(R.id.title_1);
        title_1.setText(R.string.type_operation_2);

        TextView value_1 = findViewById(R.id.value_1);
        if(title != null) value_1.setText(title);
        else value_1.setText(R.string.addition_device);

        TextView title_2 = findViewById(R.id.title_2);
        title_2.setText(R.string.date_do);

        LinearLayout priceTrip = findViewById(R.id.priceTrip);
        priceTrip.setVisibility(View.GONE);

        LinearLayout layoutOperation = findViewById(R.id.layoutOperation);
        layoutOperation.setVisibility(View.VISIBLE);

        TextView payment = findViewById(R.id.payment);
        payment.setText(R.string.device);

        LinearLayout paymentOperation = findViewById(R.id.paymentOperation);
        paymentOperation.setVisibility(View.VISIBLE);

        String text = getIntent().getStringExtra("text");
        if((text != null) && (text.equals("Ожидает записи"))) {

            TextView value_5 = findViewById(R.id.value_5);
            value_5.setText(R.string.wait_to_write_2);

            RoundButton button = findViewById(R.id.button);
            button.setVisibility(View.VISIBLE);

            ImageView card = findViewById(R.id.card);
            card.setImageResource(R.drawable.ic_visa_small);

            TextView nameCard = findViewById(R.id.nameCard);
            nameCard.setText(R.string.my_card);

            TextView numberCard = findViewById(R.id.numberCard);
            numberCard.setText(R.string.example_number_card);
        }
    }
}