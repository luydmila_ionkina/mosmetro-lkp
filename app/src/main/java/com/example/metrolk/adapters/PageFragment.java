package com.example.metrolk.adapters;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.RoundButton;
import com.example.metrolk.StaticData;
import com.example.metrolk.autorization.AutorizationActivity;
import com.example.metrolk.communication.AboutAccompaying;
import com.example.metrolk.communication.CreateAppeal;
import com.example.metrolk.communication.EnterPersonalData;
import com.example.metrolk.history.FilterOperation;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.Calendar;

public class PageFragment extends Fragment implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    public static final String ARG_PAGE = "ARG_PAGE";
    public static Context context;
    private View view = null;
    private int mPage;
    public static float density;
    public static StaticData.ActivityType typeContent;

    Calendar dateAndTime = Calendar.getInstance();
    EditText date;

    public static PageFragment newInstance(Context c, int page, float d, StaticData.ActivityType type) {

        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);

        context = c;
        density = d;
        typeContent = type;

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPage = getArguments().getInt(ARG_PAGE);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(typeContent != null) {
            switch(typeContent) {
                case infoTariff: createInfoTariff(inflater, container);
                    break;
                case ticketCard: createTicketCard(inflater, container);
                    break;
                case accompanying: createRequestAccompanying(inflater, container);
                    break;
                case answered: createAnswered(inflater, container);
                    break;
                case conversation: createAppeal(inflater, container);
                    break;
                case messagePanel: createMessagePanel(inflater, container);
                    break;
                case autoPayment: createAutoPayment(inflater, container);
                    break;
            }
        }

        return view;
    }

    public void createMessagePanel(LayoutInflater inflater, ViewGroup container) {
        view = inflater.inflate(R.layout.tab_message_panel, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerView = view.findViewById(R.id.Date);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));


        if(mPage == 0) recyclerView.setAdapter(new NewsAdapter(context, StaticData.News.all, 5));
        if(mPage == 1) recyclerView.setAdapter(new DateAdapter(context, StaticData.News.notification, density));
        if(mPage == 2) recyclerView.setAdapter(new NewsAdapter(context, StaticData.News.proposal, 7));
        if(mPage == 3) recyclerView.setAdapter(new NewsAdapter(context, StaticData.News.poll, 4));

        if(mPage != 1) {
            ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, this, density);
            new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
        }
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof NewsAdapter.ViewHolder) {

            final int deletedIndex = viewHolder.getAdapterPosition();

        }
    }

    public void createAppeal(LayoutInflater inflater, ViewGroup container){
        if(mPage == 0) {
            view = inflater.inflate(R.layout.tab_convers_1, container, false);

            RoundButton createAppeal = view.findViewById(R.id.createAppeal);
            createAppeal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, CreateAppeal.class));
                }
            });
        }
        if(mPage == 1) {
            view = inflater.inflate(R.layout.tab_convers_2, container, false);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            RecyclerView recyclerView = view.findViewById(R.id.Appeal);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(new AppealAdapter(context));
        }
    }

    public void createRequestAccompanying(LayoutInflater inflater, ViewGroup container){
        if(mPage == 0) {
            view = inflater.inflate(R.layout.tab_convers_1, container, false);

            TextView title = view.findViewById(R.id.title);
            title.setText(R.string.call_accompanying);

            TextView description = view.findViewById(R.id.description);
            description.setText(R.string.call_accompanying_3);

            ImageView iconPhone = view.findViewById(R.id.iconPhone);
            iconPhone.setVisibility(View.GONE);

            TextView more = view.findViewById(R.id.more);
            more.setText(R.string.more_2);

            LinearLayout layoutCall = view.findViewById(R.id.Call);
            layoutCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, AboutAccompaying.class));
                }
            });

            RoundButton createAppeal = view.findViewById(R.id.createAppeal);
            createAppeal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EnterPersonalData.class);
                    intent.putExtra("type", "accompanying");
                    startActivity(intent);
                }
            });
        }
        if(mPage == 1) {
            view = inflater.inflate(R.layout.tab_convers_2, container, false);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            RecyclerView recyclerView = view.findViewById(R.id.Appeal);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setAdapter(new AppealAdapter(context));
        }
    }

    public void createAnswered(LayoutInflater inflater, ViewGroup container) {
        view = inflater.inflate(R.layout.answered, container, false);
    }

    public void createTicketCard(LayoutInflater inflater, ViewGroup container) {
        view = inflater.inflate(R.layout.card_date, container, false);

        TextView date_str = view.findViewById(R.id.date_str);
        date_str.setVisibility(View.GONE);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        RecyclerView recyclerView = view.findViewById(R.id.News);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(new TariffAdapter(context, mPage));
    }

    public void createInfoTariff(LayoutInflater inflater, ViewGroup container) {
        if(mPage == 0) {
            view = inflater.inflate(R.layout.card_tariff_description, container, false);
        }
        if(mPage == 1) {
            view = inflater.inflate(R.layout.tariff_description, container, false);

            LinearLayoutManager linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            RecyclerView countTrip = view.findViewById(R.id.countTrip);
            countTrip.setLayoutManager(linearLayoutManager1);
            countTrip.setAdapter(new TripPeriodAdapter(context, 1, StaticData.Element.trip));

            LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            RecyclerView periodTrip = view.findViewById(R.id.periodTrip);
            periodTrip.setLayoutManager(linearLayoutManager2);
            periodTrip.setAdapter(new TripPeriodAdapter(context, 3, StaticData.Element.period));
        }
    }

    public void showCard(RoundedImageView imageCard) {
        View dialogView = View.inflate(context, R.layout.dialog_bottom_card, null);
        BottomSheetDialog dialog = new BottomSheetDialog(context, R.style.CustomBottomSheetDialogTheme);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        RecyclerView CardView = dialogView.findViewById(R.id.CardView);
        CardView.setLayoutManager(linearLayoutManager);
        CardView.setAdapter(new MyCardAdapter(context, context.getResources().getDisplayMetrics().density,
                StaticData.Element.bankCard, 3, imageCard, dialog));

        TextView name = dialogView.findViewById(R.id.name);
        name.setText(R.string.bank_card);

        LinearLayout enterNumberCard = dialogView.findViewById(R.id.EnterNumberCard);
        enterNumberCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //
            }
        });

        dialog.setCancelable(true);
        dialog.setContentView(dialogView);
        dialog.show();
    }

    public void createAutoPayment(LayoutInflater inflater, ViewGroup container) {
        if(mPage == 0) {
            view = inflater.inflate(R.layout.tab_main_auto_payment, container, false);

            LinearLayout layoutCardDetails = view.findViewById(R.id.card_details);
            final RoundedImageView imageCard = layoutCardDetails.findViewById(R.id.card);
            layoutCardDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) { showCard(imageCard);
                }
            });

            imageCard.setLayoutParams(new LinearLayout.LayoutParams((int) (32 * getResources().getDisplayMetrics().density + 0.5f),
                    (int) (32 * getResources().getDisplayMetrics().density + 0.5f)));
            imageCard.setImageResource(R.drawable.ic_visa_logo);
            imageCard.setBackgroundColor(StaticData.colorVisa);
            imageCard.setPadding((int) (4 * density + 0.5f), (int) (4 * density + 0.5f), (int) (4 * density + 0.5f), (int) (4 * density + 0.5f));
            imageCard.setCornerRadius((int) (6.4 * density + 0.5f));

            LinearLayout divider = layoutCardDetails.findViewById(R.id.divider);
            divider.setVisibility(View.GONE);

            LinearLayout HistoryOperation = view.findViewById(R.id.HistoryOperation);
            HistoryOperation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, FilterOperation.class);
                    intent.putExtra("type", StaticData.History.operation);
                    startActivity(intent);
                }
            });
        }
        if(mPage == 1) {
            view = inflater.inflate(R.layout.tab_settings, container, false);

            LinearLayout layoutCardDetails = view.findViewById(R.id.card_details);
            final RoundedImageView imageCard = layoutCardDetails.findViewById(R.id.card);
            layoutCardDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) { showCard(imageCard);
                }
            });

            imageCard.setLayoutParams(new LinearLayout.LayoutParams((int) (32 * getResources().getDisplayMetrics().density + 0.5f),
                    (int) (32 * getResources().getDisplayMetrics().density + 0.5f)));
            imageCard.setImageResource(R.drawable.ic_visa_logo);
            imageCard.setBackgroundColor(StaticData.colorVisa);
            imageCard.setPadding((int) (4 * density + 0.5f), (int) (4 * density + 0.5f), (int) (4 * density + 0.5f), (int) (4 * density + 0.5f));
            imageCard.setCornerRadius((int) (6.4 * density + 0.5f));

            LinearLayout cardDetails = view.findViewById(R.id.card_details);
            LinearLayout divider = cardDetails.findViewById(R.id.divider);
            divider.setVisibility(View.GONE);

            final RoundButton limitScore = view.findViewById(R.id.limitScore);
            final RoundButton timetable = view.findViewById(R.id.timetable);
            final RoundButton notification = view.findViewById(R.id.notification);
            date = view.findViewById(R.id.date);

            limitScore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    enableButton(limitScore);
                    disableButton(timetable);
                    disableButton(notification);

                    LinearLayout limitPayment = view.findViewById(R.id.limitPayment);
                    limitPayment.setVisibility(View.VISIBLE);

                    LinearLayout datePayment = view.findViewById(R.id.datePayment);
                    datePayment.setVisibility(View.GONE);

                    LinearLayout methodPayment = view.findViewById(R.id.MethodPayment);
                    methodPayment.setVisibility(View.VISIBLE);

                    RelativeLayout limitAndDate = view.findViewById(R.id.LimitAndDate);
                    limitAndDate.setVisibility(View.VISIBLE);

                    LinearLayout layoutSwitch = view.findViewById(R.id.LayoutSwitch);
                    layoutSwitch.setVisibility(View.GONE);
                }
            });

            timetable.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disableButton(limitScore);
                    enableButton(timetable);
                    disableButton(notification);

                    LinearLayout limitPayment = view.findViewById(R.id.limitPayment);
                    limitPayment.setVisibility(View.GONE);

                    LinearLayout datePayment = view.findViewById(R.id.datePayment);
                    datePayment.setVisibility(View.VISIBLE);
                    datePayment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onClickDate();
                        }
                    });

                    LinearLayout methodPayment = view.findViewById(R.id.MethodPayment);
                    methodPayment.setVisibility(View.VISIBLE);

                    RelativeLayout limitAndDate = view.findViewById(R.id.LimitAndDate);
                    limitAndDate.setVisibility(View.VISIBLE);

                    LinearLayout layoutSwitch = view.findViewById(R.id.LayoutSwitch);
                    layoutSwitch.setVisibility(View.GONE);
                }
            });

            notification.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disableButton(limitScore);
                    disableButton(timetable);
                    enableButton(notification);

                    LinearLayout methodPayment = view.findViewById(R.id.MethodPayment);
                    methodPayment.setVisibility(View.GONE);

                    RelativeLayout limitAndDate = view.findViewById(R.id.LimitAndDate);
                    limitAndDate.setVisibility(View.GONE);

                    LinearLayout layoutSwitch = view.findViewById(R.id.LayoutSwitch);
                    layoutSwitch.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    // диалоговое окно для выбора даты
    public void onClickDate() {
        new DatePickerDialog(context, d,
                dateAndTime.get(Calendar.YEAR),
                dateAndTime.get(Calendar.MONTH),
                dateAndTime.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    // установка обработчика выбора даты
    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateAndTime.set(Calendar.YEAR, year);
            dateAndTime.set(Calendar.MONTH, monthOfYear);
            dateAndTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setInitialDateTime();
        }
    };

    // установка начальных даты и времени
    private void setInitialDateTime() {
        date.setText(DateUtils.formatDateTime(context,
                dateAndTime.getTimeInMillis(), DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR));
    }

    public void enableButton(RoundButton button) {
        button.setTextColor(StaticData.colorWhite);
        button.setBackgroundColor(StaticData.colorRed);
    }

    public void disableButton(RoundButton button) {
        button.setTextColor(StaticData.colorGrayText);
        button.setBackgroundColor(StaticData.colorGray);
    }
}