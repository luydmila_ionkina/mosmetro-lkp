package com.example.metrolk.autorization;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.metrolk.MainActivity;
import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.profile.Profile;

public class FingerPrintEnter extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerprint_enter);
    }

    public void onClickMain(View view) {
        StaticData.ActivityType type = (StaticData.ActivityType) getIntent().getSerializableExtra("type");
        if(type != null) {
            switch(type) {
                case changeCode: startActivity(new Intent(FingerPrintEnter.this, Profile.class));
                    break;
                case newCode:
                    SharedPreferences.Editor editor = MainActivity.mSettings.edit();
                    editor.putString(MainActivity.AUTORIZATION, "authorized");
                    editor.apply();

                    Intent intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.putExtra("type", StaticData.ActivityType.authorized);
                    startActivity(intent);
                    break;
            }
        }
    }
}
