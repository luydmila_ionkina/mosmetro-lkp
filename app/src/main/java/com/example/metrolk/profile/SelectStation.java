package com.example.metrolk.profile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.metrolk.R;
import com.example.metrolk.adapters.StationAdapter;

public class SelectStation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_station);

        RecyclerView stationView = findViewById(R.id.Station);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        stationView.setLayoutManager(linearLayoutManager);
        stationView.setAdapter(new StationAdapter(this));
    }

    public void onClickBack(View view) {
        finish();
    }
}
