package com.example.metrolk.communication;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.metrolk.R;

public class AboutAccompaying extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization);

        ImageView question = findViewById(R.id.question);
        question.setEnabled(true);
        question.setVisibility(View.VISIBLE);

        TextView title = findViewById(R.id.title);
        title.setText(R.string.about_accompanying);

        TextView textView = findViewById(R.id.text);
        String text = "Московский метрополитен принимает заявки на сопровождение маломобильных пассажиров прямо из мобильного приложения. <br></br><br></br>Для оформления заявки на " +
                "сопровождение Вы также можете воспользоваться телефонами поддержки Центра обеспечения мобильности пассажиров: <font color='red'>+7(495)622-73-41</font> и " +
                "<font color='red'>8(800)250-73-41</font> (бесплатный вызов). <br></br><br></br>Время обслуживания пассажиров с 07:00 до 20:00.";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            textView.setText(Html.fromHtml(text,  Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        else textView.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);

        LinearLayout content = findViewById(R.id.Content);
        content.setVisibility(View.GONE);

        LinearLayout layoutButtons = findViewById(R.id.layoutButtons);
        layoutButtons.setVisibility(View.GONE);

        RelativeLayout layoutEnter = findViewById(R.id.layoutEnter);
        layoutEnter.setVisibility(View.GONE);
    }

    public void onClickBack(View view) {
        finish();
    }
}
