package com.example.metrolk.autorization;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.recovery.RecoveryAccess;
import com.example.metrolk.registration.Registration;

public class AutorizationActivity extends AppCompatActivity {

    EditText loginText;
    String login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization);

        loginText = findViewById(R.id.enter);

        final Button buttonNext = findViewById(R.id.buttonNext);
        final TextView loginHint = findViewById(R.id.loginHint);
        final LinearLayout divider = findViewById(R.id.divider);

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (loginText.length() > 0) {
                    buttonNext.setEnabled(true);
                    buttonNext.setTextColor(StaticData.colorWhite);
                    buttonNext.setBackgroundResource(R.drawable.round_message_red);
                    loginHint.setVisibility(View.VISIBLE);
                    divider.setBackgroundColor(StaticData.colorRed);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (2 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                } else {
                    buttonNext.setEnabled(false);
                    buttonNext.setTextColor(StaticData.colorGrayText);
                    buttonNext.setBackgroundResource(R.drawable.round_gray);
                    loginHint.setVisibility(View.INVISIBLE);
                    divider.setBackgroundColor(StaticData.colorGray);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.height = (int) (1 * getResources().getDisplayMetrics().density + 0.5f);
                    divider.setLayoutParams(layoutParams);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        loginText.addTextChangedListener(watcher);

         /*StaticData.showDialog(getString(R.string.user_doesnt_exist), getString(R.string.user_doesnt_exist_2),
                getString(R.string.close), null, this);*/
    }

    public void onClickLoginDescription(View view) {
        StaticData.showDialog(getString(R.string.login_description), getString(R.string.login_description_2),
                getString(R.string.close), null, this);
    }

    public void onClickNext(View view) {
        login = loginText.getText().toString();

        startActivity(new Intent(this, EnterPassword.class));
    }

    public void onClickGosServices(View view) {
        Intent intent = new Intent(this, GosServices.class);
        intent.putExtra("type", StaticData.ActivityType.mos_ru);
        startActivity(intent);
    }

    public void onClickRegistration(View view) {
        startActivity(new Intent(this, Registration.class));
    }

    public void onClickRecovery(View view) {
        Intent intent = new Intent(this, RecoveryAccess.class);
        intent.putExtra("type", StaticData.ActivityType.recoveryAccess);
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }
}
