package com.example.metrolk.payment;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.history.FilterOperation;
import com.example.metrolk.recovery.MethodRecovery;

public class PaymentFinish extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_finish);

        TextView textView = findViewById(R.id.text);

        String text = "Баланс карты тройка <b>№ 1234 567 890</b> успешно пополнен.";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            textView.setText(Html.fromHtml(text,  Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        else textView.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
    }

    public void onClickWriteToCard(View view) {
        Intent intent = new Intent(this, MethodRecovery.class);
        intent.putExtra("type", StaticData.ActivityType.writtenScore);
        startActivity(intent);
    }

    public void onClickShowOperation(View view) {
        Intent intent = new Intent(this, FilterOperation.class);
        intent.putExtra("type", StaticData.History.operation);
        startActivity(intent);
    }
}
