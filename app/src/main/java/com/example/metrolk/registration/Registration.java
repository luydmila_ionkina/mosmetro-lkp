package com.example.metrolk.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.metrolk.R;
import com.example.metrolk.StaticData;
import com.example.metrolk.autorization.GosServices;

public class Registration extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
    }

    public void onClickCreateAccount(View view) {
        Intent intent = new Intent(this, CreateUserAccount.class);
        intent.putExtra("type", StaticData.ActivityType.createUserAccount);
        startActivity(intent);
    }

    public void onClickEnterGoogle(View view) {
        Intent intent = new Intent(this, GosServices.class);
        intent.putExtra("type", StaticData.ActivityType.socialNetwork);
        startActivity(intent);
    }

    public void onClickSocialNetwork(View view) {
        Intent intent = new Intent(this, GosServices.class);
        intent.putExtra("type", StaticData.ActivityType.socialNetwork);
        startActivity(intent);
    }

    public void onClickBack(View view) {
        finish();
    }
}
